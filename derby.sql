CREATE TABLE etat
(
   id_etat INT NOT NULL GENERATED ALWAYS AS IDENTITY CONSTRAINT ETAT_PK PRIMARY KEY,
   etat VARCHAR(50) NOT NULL,
   commentaire LONG VARCHAR NOT NULL
);

CREATE TABLE metiers
(
   id_metier INT NOT NULL GENERATED ALWAYS AS IDENTITY CONSTRAINT METIER_PK PRIMARY KEY,
   metier VARCHAR(50) NOT NULL
);

CREATE TABLE recherches
(
   id_recherche int not null GENERATED ALWAYS AS IDENTITY CONSTRAINT RECHERCHES_PK PRIMARY KEY,
   raison_sociale varchar(100) not null,
   adresse LONG VARCHAR not null,
   cp varchar(5) not null,
   ville varchar(50) not null,
   telephone varchar(10) not null,
   mail varchar(50) not null,
   site varchar(100) not null,
   contact varchar(50) not null,
   commentaires LONG VARCHAR not null,
   envoye int not null,
   date_envoi date not null,
   date_rappel date not null,
   contacte int not null,
   id_metier int not null CONSTRAINT FK_METIER REFERENCES metiers(id_metier) ON DELETE RESTRICT,
   id_etat int not null default 1 CONSTRAINT FK_ETAT REFERENCES etat(id_etat) ON DELETE RESTRICT
);


CREATE INDEX metier_recherche ON recherches(id_metier);
CREATE INDEX etat_recherche ON recherches(id_etat);

INSERT INTO ETAT (ETAT,COMMENTAIRE) VALUES ('Non renseigné','nécéssaire pour l''intégrité référentielle de la base');
INSERT INTO ETAT (ETAT,COMMENTAIRE) VALUES ('Négatif','');
INSERT INTO ETAT (ETAT,COMMENTAIRE) VALUES ('Interessé','Interéssé, mais pas dans l''immédiat.\n\nA recontacter ultérieurement.');
INSERT INTO ETAT (ETAT,COMMENTAIRE) VALUES ('Effectif complet','');
INSERT INTO ETAT (ETAT,COMMENTAIRE) VALUES ('Petite structure','');

INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Deloffre','3 rue de la Clef','59800','Lille','','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Champion','60 place d''Armes','59500','Douai','0327944180','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,3,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Centre LECLERC','345 rue Louis Bréguet','59500','Douai','0327944180','','','','	',1,{d '2006-02-27'},{d '2006-03-14'},1,3,4);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Agora Presse','30 rue des Ponts de Comines','59000','Lille','0320130403','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,5,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Jadis Presse','25 rue de Paris','59800','Lille','0320130406','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,5,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Le Comptoir de la Presse','241 rue Pierre Legrand','59800','Lille','0320041399','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,5,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Sabatier Annie','10 rue d''Arras	','59000','Lille','0320527118','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Livres Libres','16 bis rue Leonars Danel ','59000','Lille','0320555805','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Dal Laurance','145 rue Colbert','59000','Lille','0320429179','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Solstices','59 rue de Gand','59000','Lille','0320555589','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Godon','16 rue Bartholomé Masurel','59800','Lille','0320315619','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Presse Sébastopol','10 place Sébastopol','59000','Lille','0320544540','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,5,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('L''Olivier','51 rue Basse','59800','Lille','0320515603','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Tirloy Père et Fils','62 rue Esquermoise','59800','Lille','0320553709','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Saint-Michel','271 rue Nationale','59800','Lille','0320546456','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Meura Hugues','25 rue de Valmy','59000','Lille','0320573644','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Folle Image','42 rue de la Clef','59000','Lille','0320550625','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Chrétienne','40 rue du Molinel','59000','Lille','0320153636','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Le Bateau Livre','154 rue Léon Gambetta','59000','Lille','0320781630','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Eldar Lille','74 rue de l''Hopital Militaire','59000','Lille','0320868293','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Champs de signes','20 rue Bartholomé Masurel','59000','Lille','0320559720','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Subway','600 boulevard de la République','59500','Douai','0327915142','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,6,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Le Pain au Levain','178 rue des Ferroniers','59500','Douai','0327877098','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('La Croissanterie','50 rue Durutte','59500','Douai','0327990922','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Delattre Dominique','150 rue de Paris','59500','Douai','0327980062','','','','',1,{d '2006-03-06'},{d '2006-03-21'},1,2,2);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Brioche de l''Étoile','67 place d''Armes','59500','Douai','0327883254','','','','',1,{d '2006-03-06'},{d '2006-03-21'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Boulangerie-Petisserie Lasege','125 rue d''Ocre','59500','Douai','0327878311','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Banette','57 rue de la Madeleine','59500','Douai','0327888372','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Aux Palais Gourmands','22 rue de la Massue','59500','Douai','0327886605','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,2,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Monoprix','1 rue de Bellain','59500','Douai','0327886262','','','','',1,{d '2006-03-07'},{d '2006-03-22'},0,3,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('ATAC','rue Jean Jaurès','59278','Escautpont','0327422002','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,3,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Astro City','22 rue Bartholomé Masurel','59000','Lille','0320559720','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Album','Centre Commercial Euralille\n','59777','Lille','0320780651','','','','',1,{d '2006-02-27'},{d '2006-03-14'},1,1,2);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Al Koursi','199 r Postes','59000','Lille','0320072462','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie du Flaneur','42 r Henri Kolb','59000','Lille','0320211449','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie de Voyageurs du Monde','147 bd Liberté Angle Place Richebé','59800','Lille','0320067630','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('La Clé Astrale','2 r Brûle Maison','59000','Lille','0320639895','','','','',1,{d '2006-02-27'},{d '2006-03-14'},1,1,2);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Colylux','31Bis r Colbert','59800','Lille','0320571298','','','','',1,{d '2006-02-27'},{d '2006-03-14'},1,4,2);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Bureautique Assistance 59','53 rue du Molinel','59800','Lille','030318831','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Bouquinerie Favereaux','19 Ter rue de l'' Hôpital Militaire','59800','Lille','0320318831','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('BD Fugue Café','5 rue Royale','59800','Lille','0320151147','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Plein Ciel Papeterie','73 r Paris','59800','Lille','0320065441','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Giard','17Bis r Saint-Jacques','59000','Lille','0320420186','','','','',1,{d '2006-02-27'},{d '2006-03-14'},1,1,5);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('La Papethèque','54 r Quesnoy','59300','Valenciennes','0327467882','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Osakatoys','15 rue Ferrand','59300','Valenciennes','0327238144','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Technique','6 avenue Villars','59300','Valenciennes','0327459547','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie du Musée des Beaux Arts','Boulevard Watteau','59300','Valenciennes','0327477474','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie du Marais','76 rue de Paris','59300','Valenciennes','0327254932','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('L''Etiquette','6 rue Tholozé','59300','Valenciennes','0327473438','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,4,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Les contes de haynaut','21 rue Beaumont','59300','Valenciennes','0327333133','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Culture Mome','48 rue de Lille','59300','Valenciennes','0327462716','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Couleurs Directes','109 rue de Famars','59300','Valenciennes','0327272432','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Arts et Distractions','5 rue de la Vieille Poissonnerie','59300','Valenciennes','0327434483','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Marché Plus','58 rue Morel','59500','Douai','0327880421','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,3,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Les Petits Papiers','126 rue St Vaast','59500','Douai','0327886156','','','','',1,{d '2006-02-27'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Maison de la Presse','35 rue Canteleu','59500','Douai','0327886294','','','M. Choquet','Appel téléphonique confirmant l''intéret du CV mais effectif complet à ce jour',1,{d '2006-02-27'},{d '2006-03-14'},1,5,3);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('La Papethèque','20 rue de Paris','59500','Douai','0327990570','','','','',1,{d '2006-02-25'},{d '2006-03-12'},1,4,2);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Manga No Kokoro','34 rue Merlin de Douai','59500','Douai','0327963443','','','','',1,{d '2006-02-25'},{d '2006-03-12'},1,1,5);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('La Clef','47 rue de St Christophe','59500','Douai','0327885293','','','','',1,{d '2006-02-25'},{d '2006-03-12'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Génération Douaisis BD','Galerie La Madeleine\nCellule R\nRue de la Madeleine','59500','Douai','0327711162','','','','',1,{d '2006-02-25'},{d '2006-03-12'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Gayant Presse','143 avenue Recklinghausen','59500','Douai','0327971408','','','','',1,{d '2006-02-25'},{d '2006-03-12'},0,5,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Bulles de Cristal','2 place du Marché aux Poissons','59500','Douai','0327711162','','','','',1,{d '2006-02-25'},{d '2006-03-14'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Art et Religion','10 rue de la Massue','59500','Douai','0327864679','','','','',1,{d '2006-02-25'},{d '2006-03-12'},0,1,1);
INSERT INTO recherches (raison_sociale,adresse,cp,ville,telephone,mail,site,contact,commentaires,envoye,date_envoi,date_rappel,contacte,id_metier,id_etat) VALUES ('Librairie Brunet','Galerie du Dauphin\n50 pl Armes','59500','Douai','0327958330','','','','',1,{d '2006-02-24'},{d '2006-03-11'},0,1,1);

