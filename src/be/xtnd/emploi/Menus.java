/*
 * Menus.java, 9 sept. 2005
 * 
 * This file is part of FilmZ.
 *
 * Copyright © 2005 Johan Cwiklinski
 *
 * File :               Menus.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

package be.xtnd.emploi;

import java.awt.Event;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.MessageFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import be.xtnd.commons.Config;
import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.HelpWindow;
import be.xtnd.commons.MessagesCommons;
import be.xtnd.commons.Print;
import be.xtnd.commons.gui.AboutBox;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.emploi.gui.IFEtat;
import be.xtnd.emploi.gui.IFMetier;
import be.xtnd.emploi.gui.IFRecherche;
import be.xtnd.emploi.objects.Recherche;

import com.jgoodies.looks.BorderStyle;
import com.jgoodies.looks.HeaderStyle;
import com.jgoodies.looks.Options;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

/**
 * 
 *
 * @author Johan Cwiklinski
 */
public class Menus {
	private GuiCommons commons = new GuiCommons(Messages.getBundle());
	private static JMenuBar rootMenuBar = new JMenuBar();
	private static JToolBar toolBar = new JToolBar();

	/** Icône de l'application au format Image */
	public static final Image APPLI_ICON = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/emploi.png")).getImage();
	/** Icône de l'application */
	public static final ImageIcon EMPLOI_ICON = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/emploi.png"));
	/** Icône des recherches */
	public static final ImageIcon RECHERCHES_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/recherche.png"));
	/** Icône des états */
	public static final ImageIcon ETATS_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/etat.png"));
	/** Icône des métiers */
	public static final ImageIcon METIERS_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/metiers.png"));
	/** Icône quiter */
	public static final ImageIcon QUIT_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/quit.png"));
	/** Icône nouveau */
	public static final ImageIcon NEW_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/new.png"));
	/** Icône modifier */
	public static final ImageIcon MODIF_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/open.png"));
	/** Icône supprimer */
	public static final ImageIcon DELETE_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/delete.png"));
	/** Icône chercher */
	public static final ImageIcon SEARCH_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/search.png"));
	/** Icône liste */
	public static final ImageIcon LIST_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/list.png"));

	/** Icône imprimer */
	public static final ImageIcon PRINT_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/print.png"));
	/** Icône préférences */
	public static final ImageIcon PREFS_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/prefs.png"));
	/** Icône aide */
	public static final ImageIcon HELP_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/help.png"));
	/** Icône fermer */
	public static final ImageIcon CLOSE_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/close.png"));
	/** Icône web */
	public static final ImageIcon WEB_IMAGE = new ImageIcon(ClassLoader.getSystemResource("be/xtnd/emploi/gui/icons/website.png"));
	/** Icône statistiques */
	//public static final ImageIcon STATS_IMAGE = new ImageIcon(ClassLoader.getSystemResource("com/trashyland/icons/statistiques.png"));
	
	/**
	 * Crée le menu
	 * @return JMenu
	 */
	public JMenuBar menus (){
		fileMenu();
		return rootMenuBar;
	}
	
	/**
	 * Crée la barre d'outils
	 * @return JToolBar
	 */
	public JToolBar toolbar(){
		menuBar();
		return toolBar;
	}
	/**
	 * fileMenu() : génère la barre de menus
	 */
	private void fileMenu(){
		rootMenuBar.putClientProperty(Options.HEADER_STYLE_KEY, HeaderStyle.BOTH);
		rootMenuBar.putClientProperty(PlasticXPLookAndFeel.BORDER_STYLE_KEY, BorderStyle.SEPARATOR);

		JMenu emploiMenu = new JMenu();
		
		JMenu recherches = new JMenu();
		JMenuItem listeRecherches = new JMenuItem();
		JMenuItem newRecherche = new JMenuItem();
		JMenuItem dropAllRecherches = new JMenuItem();
		
		JMenu etats = new JMenu();
		JMenuItem listeEtats = new JMenuItem();
		JMenuItem newEtat = new JMenuItem();
		
		JMenu corps = new JMenu();
		JMenuItem listeCorps = new JMenuItem();
		JMenuItem newCorps = new JMenuItem();
		
		//JMenuItem statsMenuItem = new JMenuItem();
		JMenuItem prefsMenuItem = new JMenuItem();
		JMenuItem printMenuItem = new JMenuItem();
		JMenuItem quitMenuItem = new JMenuItem();

		JMenu helpMenu = new JMenu();
	    JMenuItem helpMenuItem = new JMenuItem();
		JMenuItem aboutMenuItem = new JMenuItem();

		//raccourcis clavier
	    KeyStroke keystrokeExit = KeyStroke.getKeyStroke(KeyEvent.VK_Q,Event.CTRL_MASK); //exit application
	    KeyStroke keystrokeNew = KeyStroke.getKeyStroke(KeyEvent.VK_N,Event.CTRL_MASK); //new recherche
	    //KeyStroke keystrokeSearch = KeyStroke.getKeyStroke(KeyEvent.VK_F,Event.CTRL_MASK); //chercher
	    KeyStroke keystrokeHelp = KeyStroke.getKeyStroke("F1"); //aide 
	    KeyStroke keystrokePrint = KeyStroke.getKeyStroke(KeyEvent.VK_P,Event.CTRL_MASK); //impression
	    KeyStroke keystrokePrefs = KeyStroke.getKeyStroke(KeyEvent.VK_O,Event.CTRL_MASK); //préférences
	    //KeyStroke keystrokeStats = KeyStroke.getKeyStroke(KeyEvent.VK_S,Event.CTRL_MASK); //statistiques

	    //menu | Emploi
	    rootMenuBar.add(commons.menuEntry(emploiMenu, "menus.emploi", null));
	    
	    //menu | Emploi | Recherches
	    recherches.add(commons.submenuEntry(listeRecherches, LIST_IMAGE, "menus.recherches.liste", null));
	    listeRecherches.addActionListener(new ListeRecherches());
	    recherches.add(commons.submenuEntry(newRecherche, NEW_IMAGE, "menus.recherches.nouveau", keystrokeNew));
	    newRecherche.addActionListener(new AjoutRecherche());
	    emploiMenu.add(commons.menuEntry(recherches, "menus.recherches", EMPLOI_ICON));
	    recherches.add(commons.submenuEntry(dropAllRecherches, DELETE_IMAGE, "menus.recherches.vider", null));
	    dropAllRecherches.addActionListener(new VideRecherches());
	    
	    //menu | Emploi | Corps métier
	    corps.add(commons.submenuEntry(listeCorps, LIST_IMAGE, "menus.corps_metiers.liste", null));
	    listeCorps.addActionListener(new ListeMetiers());
	    corps.add(commons.submenuEntry(newCorps, NEW_IMAGE, "menus.corps_metiers.nouveau", null));
	    newCorps.addActionListener(new AjoutMetier());
	    emploiMenu.add(commons.menuEntry(corps, "menus.corps_metiers", METIERS_IMAGE));
	    
	    //menu | Emploi | Etats
	    etats.add(commons.submenuEntry(listeEtats, LIST_IMAGE, "menus.etats.liste", null));
	    listeEtats.addActionListener(new ListeEtats());
	    etats.add(commons.submenuEntry(newEtat, NEW_IMAGE, "menus.etats.nouveau", null));
	    newEtat.addActionListener(new AjoutEtat());
	    emploiMenu.add(commons.menuEntry(etats, "menus.etats", ETATS_IMAGE));
	    
	    emploiMenu.addSeparator();
	    
	    emploiMenu.add(commons.submenuEntry(prefsMenuItem, PREFS_IMAGE, "menus.preferences", keystrokePrefs));
	    prefsMenuItem.addActionListener(new PrefsAction());

	    emploiMenu.add(commons.submenuEntry(printMenuItem, PRINT_IMAGE, "menus.print", keystrokePrint));
	    printMenuItem.addActionListener(new PrintAction());
	    
	    emploiMenu.addSeparator();
	    
	    emploiMenu.add(commons.submenuEntry(quitMenuItem, QUIT_IMAGE, "menus.quit", keystrokeExit));
	    quitMenuItem.addActionListener(new QuitAction());

	    //menu | Aide	    
	    helpMenu.add(commons.submenuEntry(helpMenuItem, HELP_IMAGE, "menus.help.contents", keystrokeHelp));
	    helpMenuItem.addActionListener(new HelpAction());
	    
	    helpMenu.addSeparator();

	    helpMenu.add(commons.submenuEntry(aboutMenuItem, null, "menus.help.about", null));
	    aboutMenuItem.addActionListener(new AboutAction());

	    rootMenuBar.add(commons.menuEntry(helpMenu, "menus.help", HELP_IMAGE));
}

	/**
	 * void menuBar()
	 * mise en place de la barre de menu générale</p>
	 */
	public void menuBar(){
		toolBar.putClientProperty(Options.HEADER_STYLE_KEY, HeaderStyle.BOTH);
		toolBar.putClientProperty(PlasticXPLookAndFeel.BORDER_STYLE_KEY, BorderStyle.EMPTY);
		toolBar.setRollover(true);

		JButton listRecherches = new JButton();
		JButton listEtats = new JButton();
		JButton listCorps = new JButton();
		JButton helpButton = new JButton();
		JButton quitButton = new JButton();
		//JButton webSearch = new JButton();
		//JButton stats = new JButton();
		
		toolBar.add(commons.buildBarButton(listRecherches, "menus.recherches.liste", EMPLOI_ICON));
		listRecherches.addActionListener(new ListeRecherches());
		toolBar.add(commons.buildBarButton(listCorps, "menus.corps_metiers.liste", METIERS_IMAGE));
		listCorps.addActionListener(new ListeMetiers());
		toolBar.add(commons.buildBarButton(listEtats, "menus.etats.liste", ETATS_IMAGE));
		listEtats.addActionListener(new ListeEtats());
		
		toolBar.addSeparator();
		
		toolBar.add(commons.buildBarButton(helpButton, "menus.help.contents", HELP_IMAGE));
		helpButton.addActionListener(new HelpAction());
		
		toolBar.addSeparator();
		
		toolBar.add(commons.buildBarButton(quitButton, "menus.quit", QUIT_IMAGE));
		quitButton.addActionListener(new QuitAction());
		
		/*toolBar.addSeparator();
		
		toolBar.add(commons.buildBarButton(webSearch, "menus.web", WEB_IMAGE));
		webSearch.addActionListener(new SearchOnWeb());
		
		toolBar.addSeparator();
		
		toolBar.add(commons.buildBarButton(stats, "menus.statistics", STATS_IMAGE));
		stats.addActionListener(new StatsAction());*/
		
		toolBar.setFloatable(false);
	}
	
	/** Actions */
	class QuitAction implements ActionListener {
		/**
		 * Action quitter
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			int response = JOptionPane.showConfirmDialog(
					MainGui.desktop, 
					MessagesCommons.getString("window.messages.quit"),
					MessagesCommons.getString("window.messages.quit.title"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
    		if (response == JOptionPane.YES_OPTION) {
    		  	System.out.println("Bye, bye !");
    			System.exit(0);
    		}
		}
	}
	class HelpAction implements ActionListener {
		/**
		 * Action aide
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(HelpWindow.name,null)){
				new HelpWindow(Messages.getBundle());
			}
		}
	}
	class PrefsAction implements ActionListener {
		/**
		 * Action préférences
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(Config.name,null)){
				try {
					MainGui.conf.new ConfigWindow(EmploiMain.maingui);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	class StatsAction implements ActionListener {
		/**
		 * Action aide
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			/*if(MainGui.verifUnicite(Statistics.NAME,null)){
				new Statistics();
			}*/
		}
	}
	class PrintAction implements ActionListener {
		/**
		 * Action Imprimer
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			new Print(".config", "emplois.jrxml");
		}
	}

	class ListeRecherches implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFRecherche.LIST_NAME,null)){
				IFRecherche ifr = new IFRecherche();
				ifr.getList();
			}
		}
	}
	class AjoutRecherche implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFRecherche.NEW_NAME,null)){				
				IFRecherche.Liste liste = null;
				JInternalFrame[] frames = MainGui.desktop.getAllFrames();
				int i = 0;
				while(i < frames.length){
					if(frames[i].getName().equals(IFRecherche.LIST_NAME)){
						liste = (IFRecherche.Liste)frames[i];
						break;
					}
					i++;
				}
				if(liste == null)
					liste = new IFRecherche().new Liste();
				liste.ajoute();
			}
		}
	}
	class ListeMetiers implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFMetier.LIST_NAME,null)){
				IFMetier ifm = new IFMetier();
				ifm.getList();
			}
		}
	}
	class AjoutMetier implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFMetier.NEW_NAME,null)){
				IFMetier.Liste liste = null;
				JInternalFrame[] frames = MainGui.desktop.getAllFrames();
				int i = 0;
				while(i < frames.length){
					if(frames[i].getName().equals(IFMetier.LIST_NAME)){
						liste = (IFMetier.Liste)frames[i];
						break;
					}
					i++;
				}
				if(liste == null)
					liste = new IFMetier().new Liste();
				liste.ajoute();
			}
		}
	}
	class ListeEtats implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFEtat.LIST_NAME,null)){
				IFEtat ife = new IFEtat();
				ife.getList();
			}
		}
	}
	class AjoutEtat implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(IFEtat.NEW_NAME,null)){
				IFEtat.Liste liste = null;
				JInternalFrame[] frames = MainGui.desktop.getAllFrames();
				int i = 0;
				while(i < frames.length){
					if(frames[i].getName().equals(IFEtat.LIST_NAME)){
						liste = (IFEtat.Liste)frames[i];
						break;
					}
					i++;
				}
				if(liste == null)
					liste = new IFEtat().new Liste();
				liste.ajoute();
			}
		}
	}

	class AboutAction implements ActionListener {
		/**
		 * Action à propos
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			if(MainGui.verifUnicite(AboutBox.NAME, AboutBox.location)){
				new AboutBox(Messages.getBundle());
			}
		}
	}
	
	class VideRecherches implements ActionListener {
		/**
		 * @param e
		 */
		public void actionPerformed(ActionEvent e) {
			int response = JOptionPane.showConfirmDialog(
					MainGui.desktop, 
					Messages.getString("window.messages.delete_searches"),
					Messages.getString("window.messages.delete_searches.title"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.WARNING_MESSAGE);
    		if (response == JOptionPane.YES_OPTION) {
    			int result = 0;
    			result = Recherche.deleteAll();		

    			String strMessage = new String();
    			int typeMessage = 0;
    			String titreMessage = new String();
    			
    			if(result>0){
    				strMessage = MessageFormat.format(Messages.getString("fenetre.recherches.messages.suppression.tous"), result);
    				titreMessage = Messages.getString("fenetre.recherches.messages.suppression.titre");
    				typeMessage = JOptionPane.INFORMATION_MESSAGE;
    			}else{
    				strMessage = Messages.getString("fenetre.recherches.messages.suppression.echec.tous");
    				titreMessage = CommonsI18n.tr("Error!");
    				typeMessage = JOptionPane.ERROR_MESSAGE;
    			}
    			JOptionPane.showMessageDialog(
    					MainGui.desktop, 
    					strMessage,
    					titreMessage,
    					typeMessage);							
    			try {
    				if(!MainGui.verifUnicite(IFRecherche.LIST_NAME, null)){
    					IFRecherche.Liste liste = null;
    					JInternalFrame[] frames = MainGui.desktop.getAllFrames();
    					int i = 0;
    					while(i < frames.length){
    						if(frames[i].getName().equals(IFRecherche.LIST_NAME)){
    							liste = (IFRecherche.Liste)frames[i];
    						}
    						i++;
    					}
    					if(liste == null)
    						liste = new IFRecherche().new Liste();

    					if(result > 0)
    						liste.displayQueryResults(liste.constructQuery());
    				}
    			} catch (Exception e1) {e1.printStackTrace();}
    		}
		}
	}

}
