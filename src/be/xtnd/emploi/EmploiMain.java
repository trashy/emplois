/*
 * EmploiMain.java, 7 mars 2006
 *
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               EmploiMain.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/**
 *
 */
package be.xtnd.emploi;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;

import be.xtnd.emploi.ApplicationProperties;
import be.xtnd.commons.ShowGnu;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.emploi.gui.IFRecherche;


/**
 * @author Johan Cwiklinski
 *
 */
public class EmploiMain extends MainGui {
	private static final long serialVersionUID = 3193445179837396375L;
	private static Menus menus = new Menus();
	/** connexion base de données */
	public static Database db;
	/** */
	public static EmploiMain maingui;

	/**
	 * Main de l'application
	 * @param args
	 */
    public static void main(String[] args) {
     	String application_date = ApplicationProperties.getString("appli.date");

		SimpleDateFormat dfp = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df = new SimpleDateFormat("MMMM yyyy");

		String date = null;
		try {
			date = df.format(dfp.parse(application_date));
		} catch (ParseException e) {
			//in case of date parsing failure, we switch back to the text value
			date = application_date;
		}

		System.out.println(
    			"***** "+
				ApplicationProperties.getString("appli.name")+
				" - "+
				ApplicationProperties.getString("appli.author")+
    			" - Gnu GPL - V " + ApplicationProperties.getString("appli.version")+
				" " + date +
				" *****");

    	new ShowGnu(
    			ApplicationProperties.getString("appli.name") + " " + ApplicationProperties.getString("appli.version"),
    			date,
				ApplicationProperties.getString("appli.author"));
    	maingui = new EmploiMain();
    	IFRecherche ifr = new IFRecherche();
    	ifr.getList();
    }

    /**
     *
     *
     */
    public EmploiMain() {
    	super(
    			ApplicationProperties.getString("appli.name") + " " + ApplicationProperties.getString("appli.version"),
				new ImageIcon(ClassLoader.getSystemResource(ApplicationProperties.getString("appli.logo"))),
				menus.menus(),//menus.BuildMenuBar(),
				menus.toolbar(),//menus.buildToolBar(),
				"emploi-config.xml",
				new Dimension(1024, 768),
				ApplicationProperties.getBundle()
			);
    	super.setLocationRelativeTo(this.getParent());
    	loadDb();
    }

    /**
     * Charge la base de données
     */
    public void loadDb(){
    	db = new Database();
    	db.setConnectionParameters(
    			ResultSet.TYPE_SCROLL_INSENSITIVE,
    			ResultSet.CONCUR_UPDATABLE);
    	db.connect();
    };

    protected void finalize(){
    	db.disconnect();
    }
}
