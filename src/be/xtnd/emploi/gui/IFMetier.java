/*
 * IFMetier.java, 7 mars 2006
 * 
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               IFMetier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.gui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.TableColumnModel;

import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.MessagesCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.emploi.EmploiMain;
import be.xtnd.emploi.Menus;
import be.xtnd.emploi.Messages;
import be.xtnd.emploi.objects.Metier;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;

/**
 * @author Johan Cwiklinski
 *
 */
public class IFMetier{
	private Metier metier;
	private GuiCommons commons = new GuiCommons();
	/** Nom de la fenêtre liste */
	public static final String LIST_NAME = "liste_metiers";
	/** Nom de la fenêtre d'ajout */
	public static final String NEW_NAME = "nouveau_metier";
	private char mode;
	private JTable table;
	private ResultSetTableModelFactory factory; 
	private Liste liste;
	private DetailWindow details;
	private JTextField field_metier;

	/**
	 * Affiche la liste
	 */
	public void getList(){
		liste = new Liste();
	}

	/**
	 * Affiche la fenêtre d'ajout de métier
	 *
	 */
	public void nouveauMetier(){
		metier = new Metier();
		mode = Database.NEW;
		details = new DetailWindow();
	}
	
	/**
	 * Affiche la fenêtre de modification du métier sélectionné
	 * @param id identifiant du métier à modifier
	 */
	public void modifieMetier(Integer id){
		metier = new Metier(id);
		mode =Database.MODIF;
		details = new DetailWindow();
	}

	/**
	 * Supprimer un coprs de métier 
	 * @param id identifiant du corps de métier à supprimer
	 */
	public void supprimeMetier(Integer id){
		metier = new Metier(id);
		Object[] args = {metier.getMetier()};
		int result = 0;
		int response = JOptionPane.showConfirmDialog(
				MainGui.desktop, 
				MessageFormat.format(Messages.getString("fenetre.metiers.messages.suppression.confirmation"), args),
				Messages.getString("fenetre.metiers.messages.suppression.titre"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);

		if (response == JOptionPane.YES_OPTION) {
			result = metier.delete(id);		

			String strMessage = new String();
			int typeMessage = 0;
			String titreMessage = new String();
			
			if(result>0){
				strMessage = MessageFormat.format(
						Messages.getString("fenetre.metiers.messages.suppression"),
						args);
				titreMessage = Messages.getString("fenetre.metiers.messages.suppression.titre");
				typeMessage = JOptionPane.INFORMATION_MESSAGE;
			}else{
				if(result==-2){
					strMessage = MessageFormat.format(
							Messages.getString("fenetre.metiers.messages.fk"),
							args);
					titreMessage = Messages.getString("fenetre.metiers.messages.fk.titre");
					typeMessage = JOptionPane.WARNING_MESSAGE;					
				}else{
					strMessage = MessageFormat.format(
							Messages.getString("fenetre.metiers.messages.suppression.echec"),
							args);
					titreMessage = CommonsI18n.tr("Error!");
					typeMessage = JOptionPane.ERROR_MESSAGE;
				}
			}
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					strMessage,
					titreMessage,
					typeMessage);							
			try {
				if(!MainGui.verifUnicite(IFMetier.LIST_NAME, null)){
		        	liste.storeSort = liste.sorter.getSortingColumns();
					liste.displayQueryResults(Metier.LIST_QUERY);
				}
			} catch (Exception e1) {e1.printStackTrace();}
		}
		
	}

	/**
	 * Vérifie les champs obligatoires
	 * @return true si les entrées sont valides, flase sinon
	 */
	protected boolean verifEntrees(){
		boolean valid = true;
		StringBuffer sb = new StringBuffer();
		sb.append(MessagesCommons.getString("window.messages.required"));

		if(field_metier.getText().trim().equals("")){
			sb.append("\n- "+Messages.getString("fenetre.metiers.messages.requis.metier"));
			if(valid) field_metier.requestFocus();
			valid = false;
		}

		if(!valid){
			JOptionPane.showMessageDialog(
				MainGui.desktop, 
				sb.toString(),
				CommonsI18n.tr("Error!"),
				JOptionPane.ERROR_MESSAGE);			
		}
		return valid;
	}

	/**
	 * Enregistre les données du métier
	 *
	 */
	protected void queries(){
		boolean nouveau = false;
		String messageTitre = new String();
		int boite = 0;
		String message = new String();
		
		metier.setMetier(field_metier.getText());
		
		if(mode==Database.NEW){
			messageTitre = Messages.getString("fenetre.metiers.messages.ajout.titre");
			nouveau = true;
		}else if(mode==Database.MODIF){
			messageTitre = Messages.getString("fenetre.metiers.messages.modification.titre");
			nouveau = false;
		}
		
		metier.enregistrer(nouveau);

		Object[] args = {metier.getMetier()};
		if(metier.getAffectedRows()>0){
			boite = JOptionPane.INFORMATION_MESSAGE+JOptionPane.OK_OPTION;
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.metiers.messages.ajout"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.metiers.messages.modification"), args);
			}
			details.dispose();
			try {
				if(!MainGui.verifUnicite(IFMetier.LIST_NAME, null)){
					if( liste == null){
						JInternalFrame[] frames = MainGui.desktop.getAllFrames();
						int i = 0;
						while(i < frames.length){
							if(frames[i].getName().equals(IFMetier.LIST_NAME)){
								liste = (IFMetier.Liste)frames[i];
								break;
							}
							i++;
						}
					}

					liste.displayQueryResults(Metier.LIST_QUERY);
				}
			} catch (Exception e1) {e1.printStackTrace();}
		}else{
			boite = JOptionPane.WARNING_MESSAGE+JOptionPane.OK_OPTION;
			if(metier.getSqlMessage().equals("")) metier.setSqlMessage(MessagesCommons.getString("window.messages.noerror"));
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.metiers.messages.ajout.echec"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.metiers.messages.modification.echec"), args);
			}
			message = message + "\n("+metier.getSqlMessage()+")";
		}
		JOptionPane.showMessageDialog(MainGui.desktop, message, messageTitre, boite);
	}	

	
	protected class DetailWindow extends EscapeInternalFrame{
		private static final long serialVersionUID = -3516085133189643664L;
		private TitledSeparator titre;
		
		protected DetailWindow(){
			super();
            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
			fenetreChamps();
		}
					
		void fenetreChamps(){
			FormPanel pane = new FormPanel( "be/xtnd/emploi/gui/metier.jfrm" );
			
			titre = (TitledSeparator)pane.getComponentByName("titre");
			
			if(mode==Database.NEW){
				titre.setText(Messages.getString("fenetre.metiers.titres.nouveau"));
				setName(NEW_NAME);				
			}else{
				Object[] args = {metier.getMetier()};
				String strtitre = MessageFormat.format(Messages.getString("fenetre.metiers.titres.modifier"), args);
				titre.setText(strtitre);
				setName("metier_"+metier.getId());				
			}

			field_metier = pane.getTextField("texte");
			field_metier.setText(metier.getMetier());
	        
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setClosable(true);
			setMaximizable(true);
			setIconifiable(true);
			
			add(pane);
			
			commons.createButtonBar(this,new OkayEvent());
			
			pack();
			setVisible(true);
			setLocation(MainGui.centerOnDesktop(getSize()));
			MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
			try {
				setSelected(true);
			} catch (PropertyVetoException e) {}			
		}
						
		class OkayEvent implements ActionListener{
			/**
			 * @param e ActionEvent
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees())
					queries();
			}		
		}
	}

	
	/**
	 * 
	 */
	public class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = 5963677065711875456L;
		private Integer id_selected;
		private JPopupMenu popup;
		private TitledSeparator titre;
		/** Ordre de tri des colonnes */
		public List<Object> storeSort = null;
		/** TableSorter de la JTable */
		public TableSorter sorter; 

		/**
		 * Affiche la liste des métiers avec les paramètres par défaut.
		 *
		 */
		public Liste(){		
			this(new ResultSetTableModelFactory(EmploiMain.db));

			setLocation(MainGui.centerOnDesktop(getSize()));
			setVisible(true);
			MainGui.desktop.add(this);	
		}
		
		/**
		 * Affiche la liste des films à la position spécifiée
		 * @param position point position de la fenêtre à l'affichage
		 */
		public Liste(Point position){
			this(new ResultSetTableModelFactory(EmploiMain.db));

			if(position==null) position = MainGui.centerOnDesktop(getSize());
			setLocation(position);
			setVisible(true);
			MainGui.desktop.add(this);	
		}
		
		/**
		 * Affiche la liste des métiers avec le ResultSet associé
		 * @param f ResultSetTableModelFactory à afficher
		 * 
		 * @see ResultSetTableModelFactory
		 */
		public Liste(ResultSetTableModelFactory f) {
			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(LIST_NAME);
			setSize(300, 350);

            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
			metier = new Metier();
			
			FormPanel pane = new FormPanel( "be/xtnd/emploi/gui/liste.jfrm" );
			getContentPane().add(pane);
			
			titre = (TitledSeparator)pane.getComponentByName("titre");
			titre.setText(Messages.getString("fenetre.metiers.titres.liste"));

			factory = f;
			table = pane.getTable("table");
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						id_selected = new Integer(table.getValueAt(table.rowAtPoint(e.getPoint()),0).toString());
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
		            if (e.getClickCount() == 2) {// double-click
		            	id_selected = new Integer(table.getValueAt(table.rowAtPoint(e.getPoint()),0).toString());
						if(MainGui.verifUnicite("metier_"+id_selected,null)){
							modifieMetier(id_selected);
						}
					}

				}
			});
			displayQueryResults(Metier.LIST_QUERY);
			
			popup = new JPopupMenu();
			popupEntries(popup, Messages.getString("window.table_popup.new"),"new",Menus.NEW_IMAGE);
			popupEntries(popup, Messages.getString("window.table_popup.modif"),"detailled",Menus.EMPLOI_ICON);
			popup.addSeparator();
			popupEntries(popup, Messages.getString("window.table_popup.del"),"del",Menus.DELETE_IMAGE);

			table.add(popup);
		}		
		
		/**
		 * Affiche les données d'une requête sql dans une JTable
		 * @param q requête à exécuter
		 */
		public void displayQueryResults(final String q) {
			MainGui.statusBar.setText("Contacting database...");

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						table.setModel(factory.getResultSetTableModel(q));
						
				        sorter = new TableSorter(table.getModel());
				        if(storeSort!=null){
				        	sorter.setSortingColumns(storeSort);
				        }
				        
				        table.setModel(sorter);
				        sorter.setTableHeader(table.getTableHeader());
				        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));
						
				        TableColumnModel columnModel = table.getColumnModel();
						columnModel.getColumn(0).setPreferredWidth(0);
						columnModel.getColumn(0).setMinWidth(0);
						columnModel.getColumn(0).setMaxWidth(0);

						titre.setText(Messages.getString("fenetre.metiers.titres.liste")+" ("+table.getRowCount()+")");

						MainGui.verifUnicite(IFMetier.LIST_NAME, null);
						MainGui.statusBar.setText("");
					} catch (SQLException ex) {
						MainGui.statusBar.setText("Error contacting database !");
						JOptionPane.showMessageDialog(Liste.this,
								new String[] { // Display a 2-line message
								ex.getClass().getName() + ": ", ex.getMessage() });
						MainGui.statusBar.setText("");					
					}
				}
			});
		}	
	
		/**
		 * Affiche la fenêtre d'ajout de métier
		 * Utile pour appels depuis une classe 
		 * externe (notamment pour le raccourci d'ajout
		 * de métier)
		 */
		public void ajoute(){
			nouveauMetier();
		}
		
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(MainGui.verifUnicite(NEW_NAME,null)){
					nouveauMetier();
				}
			}else if(command.equals("del")){
				supprimeMetier(id_selected);
			}else if(command.equals("detailled")){
				if(MainGui.verifUnicite("metier_"+id_selected,null)){
					modifieMetier(id_selected);
				}
			}
		}	
	}
}
