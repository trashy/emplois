/*
 * IFRecherche.java, 7 mars 2006
 * 
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               IFRecherche.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.gui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.MessagesCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.db.ResultSetTableModelFactory;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.gui.TableSorter;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.emploi.EmploiMain;
import be.xtnd.emploi.Menus;
import be.xtnd.emploi.Messages;
import be.xtnd.emploi.objects.Etat;
import be.xtnd.emploi.objects.Historique;
import be.xtnd.emploi.objects.Metier;
import be.xtnd.emploi.objects.Recherche;
import be.xtnd.validators.MailFieldVerifier;
import be.xtnd.validators.MaskFieldVerifier;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;

/**
 * @author Johan Cwiklinski
 *
 */
public class IFRecherche{
	protected Recherche recherche;
	protected GuiCommons commons = new GuiCommons();
	/** Nom de la fenêtre liste */
	public static final String LIST_NAME = "liste_recherches";
	/** Nom de la fenêtre d'ajout */
	public static final String NEW_NAME = "nouvelle_recherche";
	protected char mode;
	private Liste liste;
	private DetailWindow details;
	
	private JTable table, history_table;
	private ResultSetTableModelFactory factory;
	
	private JTextField rs_search, ville_search;
	private JComboBox metier_search, etats_search;
	private JCheckBox envoye_search, modifie_search/*, contacte_search*/;
	private JDateChooser date_debut_search, date_fin_search, date_debut_modif, date_fin_modif;
	
	private Date current_date;
	
	//éléments detailwindow
	private JComboBox metiers, etats;
	private JTextField rs, cp, ville, mail, site, contact, poste_contact;
	private JFormattedTextField tel;
	private JTextArea adresse, commentaires;
	private JCheckBox envoye/*, contacte*/;
	private JDateChooser creation, modification, rappel;
	private JButton empty_histo;
	/*private JButton add_suivi, del_suivi, modif_suivi;*/

	/**
	 * 
	 */
	public IFRecherche(){
		current_date = Calendar.getInstance().getTime();
	}
	
	/**
	 * Affiche la liste
	 */
	public void getList(){
		liste = new Liste();
	}

	/**
	 * Affiche la fenêtre d'ajout de recherche
	 *
	 */
	public void nouvelleRecherche(){
		recherche = new Recherche();
		mode = Database.NEW;
		details = new DetailWindow();
	}
	
	/**
	 * Affiche la fenêtre de modification du film sélectionné
	 * @param id identifiant du film à modifier
	 */
	public void modifieRecherche(Integer id){
		recherche = new Recherche(id);
		mode =Database.MODIF;
		details = new DetailWindow();
	}
	
	/**
	 * Supprimer une recherche 
	 * @param id identifiant de la recherche à supprimer
	 */
	public void supprimeRecherche(Integer id){
		recherche = new Recherche(id);
		Object[] args = {recherche.getRaison_sociale()};
		int result = 0;
		int response = JOptionPane.showConfirmDialog(
				MainGui.desktop, 
				MessageFormat.format(Messages.getString("fenetre.recherches.messages.suppression.confirmation"), args),
				Messages.getString("fenetre.recherches.messages.suppression.titre"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);

		if (response == JOptionPane.YES_OPTION) {
			result = recherche.delete(id);		

			String strMessage = new String();
			int typeMessage = 0;
			String titreMessage = new String();
			
			if(result>0){
				strMessage = MessageFormat.format(
						Messages.getString("fenetre.recherches.messages.suppression"),
						args);
				titreMessage = Messages.getString("fenetre.recherches.messages.suppression.titre");
				typeMessage = JOptionPane.INFORMATION_MESSAGE;
			}else{
				strMessage = MessageFormat.format(
						Messages.getString("fenetre.recherches.messages.suppression.echec"),
						args);
				titreMessage = CommonsI18n.tr("Error!s");
				typeMessage = JOptionPane.ERROR_MESSAGE;
			}
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					strMessage,
					titreMessage,
					typeMessage);							
			try {
				if(!MainGui.verifUnicite(IFRecherche.LIST_NAME, null)){
		        	liste.storeSort = liste.sorter.getSortingColumns();
					liste.displayQueryResults(liste.constructQuery());
				}
			} catch (Exception e1) {e1.printStackTrace();}
		}
		
	}
	
	/**
	 * Vérifie les champs obligatoires
	 * @return true si les entrées sont valides, flase sinon
	 */
	protected boolean verifEntrees(){
		boolean valid = true;
		StringBuffer sb = new StringBuffer();
		sb.append(MessagesCommons.getString("window.messages.required"));

		if(metiers.getSelectedIndex()==0){
			sb.append("\n- "+Messages.getString("fenetre.recherches.requis.metier"));
			if(valid) metiers.requestFocus();
			valid = false;
		}
		if(rs.getText().trim().equals("")){
			sb.append("\n- "+Messages.getString("fenetre.recherches.requis.raison_sociale"));
			if(valid) rs.requestFocus();
			valid = false;
		}
		if(ville.getText().trim().equals("")){
			sb.append("\n- "+Messages.getString("fenetre.recherches.requis.ville"));
			if(valid) ville.requestFocus();
			valid = false;
		}
		if(etats.getSelectedIndex()==0){
			sb.append("\n- "+Messages.getString("fenetre.recherches.requis.etat"));
			if(valid) etats.requestFocus();
			valid =false;
		}
		
		if(!valid){
			JOptionPane.showMessageDialog(
				MainGui.desktop, 
				sb.toString(),
				CommonsI18n.tr("Error!"),
				JOptionPane.ERROR_MESSAGE);			
		}
		return valid;
	}

	/**
	 * Enregistre les données de la recherche
	 *
	 */
	protected void queries(){
		boolean nouveau = false;
		String messageTitre = new String();
		int boite = 0;
		String message = new String();
		
		recherche.setMetier(new Metier(metiers.getSelectedItem().toString()));
		recherche.setRaison_sociale(rs.getText());
		recherche.setAdresse(adresse.getText());
		recherche.setCp(cp.getText());
		recherche.setVille(ville.getText());
		recherche.setTelephone(tel.getText());
		recherche.setMail(mail.getText());
		recherche.setSite(site.getText());
		recherche.setContact(contact.getText());
		recherche.setPosteContact(poste_contact.getText());
		recherche.setCommentaires(commentaires.getText());		
		recherche.setEnvoye(envoye.isSelected());
		if(recherche.isEnvoye()) recherche.setDate_creation(creation.getDate());
		// TODO: what to do ?
		if(recherche.isEnvoye()) recherche.setDate_modif(modification.getDate());
		if(recherche.isEnvoye()) recherche.setDate_rappel(rappel.getDate());
		if(recherche.isEnvoye()){
			recherche.setEtat(new Etat(etats.getSelectedItem().toString()));
		}else{
			recherche.setEtat(new Etat(Etat.DEFAULT));
		}
		
		if(mode==Database.NEW){
			messageTitre = Messages.getString("fenetre.recherches.messages.ajout.titre");
			nouveau = true;
		}else if(mode==Database.MODIF){
			messageTitre = Messages.getString("fenetre.recherches.messages.modification.titre");
			nouveau = false;
		}
		
		recherche.enregistrer(nouveau);

		Object[] args = {recherche.getRaison_sociale()};
		if(recherche.getAffectedRows()>0){
			boite = JOptionPane.INFORMATION_MESSAGE+JOptionPane.OK_OPTION;
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.recherches.messages.ajout"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.recherches.messages.modification"), args);
			}
			details.dispose();
			try {
				if(!MainGui.verifUnicite(IFRecherche.LIST_NAME, null)){
					if( liste == null ){
						JInternalFrame[] frames = MainGui.desktop.getAllFrames();
						int i = 0;
						while(i < frames.length){
							if(frames[i].getName().equals(IFRecherche.LIST_NAME)){
								liste = (IFRecherche.Liste)frames[i];
								break;
							}
							i++;
						}
					}
					String qry = liste.constructQuery();
		        	liste.storeSort = liste.sorter.getSortingColumns();
					liste.displayQueryResults(qry);
				}
			} catch (Exception e1) {e1.printStackTrace();}
			
			//On propose ici d'ajouter un commentaire à l'historique
			if( recherche.hasChanged() || Historique.getCount(recherche.getId()) < 1 ){
				Historique h = new Historique();
				h.setIdRecherche(recherche.getId());
				h.setEtat(recherche.getEtat());
				h.setCommentaire("");
				h.setDate(new Date());
				int response = JOptionPane.showConfirmDialog(
						MainGui.desktop, 
						MessageFormat.format(Messages.getString("fenetre.recherches.messages.modifie"), args),
						Messages.getString("fenetre.recherches.messages.modification.titre"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if( response == JOptionPane.YES_OPTION){
					new IFHistorique().nouvelHistorique(h);
				} else {
					h.enregistrer(true);
				}
			}
		}else{
			boite = JOptionPane.WARNING_MESSAGE+JOptionPane.OK_OPTION;
			if(recherche.getSqlMessage().equals("")) recherche.setSqlMessage(MessagesCommons.getString("window.messages.noerror"));
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.recherches.messages.ajout.echec"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.recherches.messages.modification.echec"), args);
			}
			message = message + "\n("+recherche.getSqlMessage()+")";
		}
		JOptionPane.showMessageDialog(MainGui.desktop, message, messageTitre, boite);
	}	

	/**
	 * (Dés)active les champs JDateChooser
	 * @param elt élément JDateChooser à modifier
	 * @param tostate état 
	 */
	private void datesState(JDateChooser elt, boolean tostate){
		elt.setEnabled(false);
		Component[] begin_comps = elt.getComponents();
		for (Component comp : begin_comps) {
			comp.setEnabled(tostate);
		}
	}
	
	protected class DetailWindow extends EscapeInternalFrame{
		private static final long serialVersionUID = -3516085133189643664L;
		private TitledSeparator titre; 
		private MaskFormatter fmPhone;
		/** Ordre de tri des colonnes */
		public List<Object> storeSort = null;
		/** TableSorter de la JTable */
		public TableSorter sorter; 
		private ResultSetTableModelFactory facto;
		private Integer id_selected;
		private JPopupMenu popup;
		
		protected DetailWindow(){
			super();
            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
			fenetreChamps();
		}
					
		void fenetreChamps(){
			FormPanel pane = new FormPanel( "be/xtnd/emploi/gui/emploi.jfrm" );
			
			titre = (TitledSeparator)pane.getComponentByName("titre");

			if(mode==Database.NEW){
				titre.setText(Messages.getString("fenetre.recherches.titres.nouveau"));
				super.setName(NEW_NAME);				
			}else{
				Object[] args = {recherche.getRaison_sociale()};
				String strtitre = MessageFormat.format(Messages.getString("fenetre.recherches.titres.modifier"), args);
				titre.setText(strtitre);
				super.setName("recherche_"+recherche.getId());				
			}

			metiers = pane.getComboBox("metier");
			commons.buildCombo(metiers, Metier.listing());
			if(mode==Database.MODIF){
				metiers.setSelectedItem(recherche.getMetier().getMetier());
			}
			
			rs = pane.getTextField("raison_sociale");
			rs.setText(recherche.getRaison_sociale());
			
			adresse = (JTextArea)pane.getComponentByName("adresse");
			adresse.setLineWrap(true);
			adresse.setWrapStyleWord(true);
			adresse.setText(recherche.getAdresse());
			
			cp = pane.getTextField("code_postal");
			cp.setText(recherche.getCp());
			
			ville = pane.getTextField("ville");
			ville.setText(recherche.getVille());
			
			try {
				fmPhone = new MaskFormatter("## ## ## ## ##");
			} catch (ParseException e) {e.printStackTrace();}

			tel = (JFormattedTextField)pane.getComponentByName("telephone");
			tel.setFormatterFactory(new DefaultFormatterFactory(fmPhone));
			tel.setInputVerifier(new MaskFieldVerifier(recherche.getTelephone(), fmPhone.getMask()));
			if(!recherche.getTelephone().trim().equals("")){
				tel.setText(recherche.getTelephone());
				try {
					tel.commitEdit();
				} catch (ParseException e1) {e1.printStackTrace();}
			}
			
			mail = pane.getTextField("email");
			mail.setText(recherche.getMail());
			mail.setInputVerifier(new MailFieldVerifier(recherche.getMail()));
			
			site = pane.getTextField("site_web");
			site.setText(recherche.getSite());
			//FIXME vérification de la validité du lien url
			
			contact = pane.getTextField("contact");
			contact.setText(recherche.getContact());
			
			poste_contact = pane.getTextField("contact_poste");
			poste_contact.setText(recherche.getPosteContact());
			
			commentaires = (JTextArea)pane.getComponentByName("commentaires");
			commentaires.setText(recherche.getCommentaires());
			commentaires.setLineWrap(true);
			commentaires.setWrapStyleWord(true);
			
			envoye = pane.getCheckBox("envoye");
			envoye.setSelected(recherche.isEnvoye());
			envoye.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					datesState(creation, envoye.isSelected());
					datesState(modification, envoye.isSelected());
					datesState(rappel, envoye.isSelected());
					etats.setEnabled(envoye.isSelected());
					history_table.setEnabled(envoye.isSelected());
				}
			});
			
			creation = (JDateChooser)pane.getComponentByName("date_creation");
			creation.setDateFormatString(Messages.getString("emploi.date.format"));
			creation.setDate((recherche.isEnvoye())?recherche.getDate_creation():current_date);
			
			modification = (JDateChooser)pane.getComponentByName("date_modif");
			modification.setDateFormatString(Messages.getString("emploi.date.format"));
			modification.setDate((recherche.isEnvoye())?recherche.getDate_modif():current_date);
			
			rappel = (JDateChooser)pane.getComponentByName("date_rappel");
			rappel.setDateFormatString(Messages.getString("emploi.date.format"));
			Calendar cal = new GregorianCalendar();
			cal.setTime(current_date);
			cal.add(Calendar.DATE, +15);
			rappel.setDate((recherche.isEnvoye())?recherche.getDate_rappel():cal.getTime());
			
			if(mode==Database.NEW || !recherche.isEnvoye()){
				datesState(creation, false);
				datesState(modification, false);
				datesState(rappel, false);
			}
			
			facto = new ResultSetTableModelFactory(EmploiMain.db);
			history_table = pane.getTable("suivi_table");
			/* FIXME: peupler la table */
			history_table.getTableHeader().setReorderingAllowed(false);
			history_table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						id_selected = new Integer(history_table.getValueAt(history_table.rowAtPoint(e.getPoint()),0).toString());
						history_table.changeSelection(history_table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
		            if (e.getClickCount() == 2) {// double-click
		            	id_selected = new Integer(history_table.getValueAt(history_table.rowAtPoint(e.getPoint()),0).toString());
						if(MainGui.verifUnicite("historique_" + recherche.getId() + "_" + id_selected,null)){
							new IFHistorique().modifieHistorique(id_selected);
						}
					}

				}
			});
			if( mode == Database.MODIF){
				displayQueryResults(Historique.getListQuery(recherche.getId()));
			}
			if(mode==Database.NEW || !recherche.isEnvoye()){
				history_table.setEnabled(false);
			}
			
			popup = new JPopupMenu();
			/*popupEntries(popup, Messages.getString("window.table_popup.new"),"new",Menus.NEW_IMAGE);*/
			popupEntries(popup, Messages.getString("window.table_popup.modif"),"detailled",Menus.EMPLOI_ICON);
			popup.addSeparator();
			popupEntries(popup, Messages.getString("window.table_popup.del"),"del",Menus.DELETE_IMAGE);

			history_table.add(popup);
			
			empty_histo = (JButton) pane.getButton("empty_histo");
			empty_histo.setText("");
			empty_histo.setToolTipText(Messages.getString("fenetre.recherches.historique.vider.titre"));
			empty_histo.addActionListener(new EmptyHistoryEvent());
			if(mode==Database.NEW || !recherche.isEnvoye() || Historique.getCount(recherche.getId()) < 1 ){
				empty_histo.setEnabled(false);
			}

			/*add_suivi = (JButton) pane.getButton("add_suivi");
			add_suivi.setText("");
			add_suivi.setMnemonic(Messages.getString("fenetre.recherches.historique.ajouter.mnemonic").charAt(0));
			add_suivi.setToolTipText(Messages.getString("fenetre.recherches.historique.ajouter.titre"));
			add_suivi.addActionListener(new AddHistoryEvent());
			if(mode==Database.NEW || !recherche.isEnvoye()){
				add_suivi.setEnabled(false);
			}*/
			
			/*modif_suivi = (JButton) pane.getButton("modif_suivi");
			modif_suivi.setText("");
			modif_suivi.setMnemonic(Messages.getString("fenetre.recherches.historique.modifier.mnemonic").charAt(0));
			modif_suivi.setToolTipText(Messages.getString("fenetre.recherches.historique.modifier.titre"));
			modif_suivi.addActionListener(new ModifHistoryEvent());
			if(mode==Database.NEW || !recherche.isEnvoye()){
				modif_suivi.setEnabled(false);
			}*/

			/*del_suivi = (JButton) pane.getButton("del_suivi");
			del_suivi.setText("");
			del_suivi.setMnemonic(Messages.getString("fenetre.recherches.historique.supprimer.mnemonic").charAt(0));
			del_suivi.setToolTipText(Messages.getString("fenetre.recherches.historique.supprimer.titre"));
			if(mode==Database.NEW || !recherche.isEnvoye()){
				del_suivi.setEnabled(false);
			}*/

			/*contacte = pane.getCheckBox("contacte");
			contacte.setSelected(recherche.isContacte());
			contacte.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					history_table.setEnabled(contacte.isSelected());
					add_suivi.setEnabled(contacte.isSelected());
					modif_suivi.setEnabled(contacte.isSelected());
					del_suivi.setEnabled(contacte.isSelected());
					//etats.setEnabled(contacte.isSelected());
				}
			});*/
			
			etats = pane.getComboBox("etat");
			commons.buildCombo(etats, Etat.listing());
			if(mode==Database.MODIF){
				etats.setSelectedItem(recherche.getEtat().getEtat());
			}
			if(mode==Database.NEW || !recherche.isEnvoye()){
				etats.setEnabled(false);
			}
			
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setClosable(true);
			setMaximizable(true);
			setIconifiable(true);
			
			add(pane);
			
			commons.createButtonBar(this,new OkayEvent());
			
			pack();
			setVisible(true);
			setLocation(MainGui.centerOnDesktop(getSize()));
			MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
			try {
				setSelected(true);
			} catch (PropertyVetoException e) {}
		}
				
		class OkayEvent implements ActionListener{
			/**
			 * @param e ActionEvent
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees())
					queries();
			}		
		}
		
		/*class AddHistoryEvent implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				if(MainGui.verifUnicite(IFHistorique.NEW_NAME,null)){
					new IFHistorique().nouvelHistorique(recherche.getId());
				}
			}
			
		}*/

		class EmptyHistoryEvent implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] args = {recherche.getRaison_sociale()};
				int result = 0;
				int response = JOptionPane.showConfirmDialog(
						MainGui.desktop, 
						MessageFormat.format(Messages.getString("fenetre.historique.messages.vider.confirmation"), args),
						Messages.getString("fenetre.historique.messages.vider.titre"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);

				if (response == JOptionPane.YES_OPTION) {
					result = Historique.empty(recherche.getId());
					String strMessage = new String();
					int typeMessage = 0;
					String titreMessage = new String();
					
					if(result>0){
						strMessage = MessageFormat.format(
								Messages.getString("fenetre.historique.messages.vider"),
								args);
						titreMessage = Messages.getString("fenetre.historique.messages.vider.titre");
						typeMessage = JOptionPane.INFORMATION_MESSAGE;
					}else{
						strMessage = MessageFormat.format(
								Messages.getString("fenetre.historique.messages.vider.echec"),
								args);
						titreMessage = CommonsI18n.tr("Error!");
						typeMessage = JOptionPane.ERROR_MESSAGE;
					}
					JOptionPane.showMessageDialog(
							MainGui.desktop, 
							strMessage,
							titreMessage,
							typeMessage);

					displayQueryResults(Historique.getListQuery(recherche.getId()));
				}
			}
		
		}

		class ModifHistoryEvent implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				int current_id = (Integer) history_table.getValueAt(history_table.getSelectedRow(), 0);
				if(MainGui.verifUnicite("historique_" + recherche.getId() + "_" + current_id,null)){
					new IFHistorique().modifieHistorique(current_id);
				}
			}
			
		}

		/**
		 * Affiche les données d'une requête sql dans une JTable
		 * @param q requête à exécuter
		 */
		public void displayQueryResults(final String q) {
			MainGui.statusBar.setText("Contacting database...");

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						history_table.setModel(facto.getResultSetTableModel(q));
				        sorter = new TableSorter(history_table.getModel());
				        if(storeSort!=null){
				        	sorter.setSortingColumns(storeSort);
				        }

				        history_table.setModel(sorter);
				        sorter.setTableHeader(history_table.getTableHeader());
				        history_table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));
						
				        TableColumnModel columnModel = history_table.getColumnModel();
						columnModel.getColumn(0).setPreferredWidth(0);
						columnModel.getColumn(0).setMinWidth(0);
						columnModel.getColumn(0).setMaxWidth(0);
						columnModel.getColumn(2).setPreferredWidth(130);
						columnModel.getColumn(2).setMinWidth(130);
						columnModel.getColumn(2).setMaxWidth(130);
						columnModel.getColumn(2).setCellRenderer(new DateTimeRenderer());

						MainGui.verifUnicite("recherche_"+recherche.getId(), null);
						MainGui.statusBar.setText("");
					} catch (SQLException ex) {
						MainGui.statusBar.setText("Error contacting database !");
						JOptionPane.showMessageDialog(DetailWindow.this,
								new String[] { // Display a 2-line message
								ex.getClass().getName() + ": ", ex.getMessage() });
						MainGui.statusBar.setText("");					
					}
				}
			});
		}
		
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("del")){
				new IFHistorique().supprimeHistorique(id_selected);
			}else if(command.equals("detailled")){
				if(MainGui.verifUnicite("historique_"+id_selected,null)){
					new IFHistorique().modifieHistorique(id_selected);
				}
			}
		}
	}
	
	/**
	 * 
	 */
	public class Liste extends EscapeInternalFrame{
		private static final long serialVersionUID = 5635082151587360418L;
		private Integer id_selected;
		private JPopupMenu popup;
		/* list header */
		private TitledSeparator titre;
		private JButton search, reinit;
		/** Ordre de tri des colonnes */
		public List<Object> storeSort = null;
		/** TableSorter de la JTable */
		public TableSorter sorter; 

		/**
		 * Affiche la liste des films avec les paramètres par défaut.
		 *
		 */
		public Liste(){		
			this(new ResultSetTableModelFactory(EmploiMain.db));

			setLocation(MainGui.centerOnDesktop(getSize()));
			setVisible(true);
			MainGui.desktop.add(this);
			try {
				setMaximum(true);
			} catch (PropertyVetoException e2) {}
		}
		
		/**
		 * Affiche la liste des films à la position spécifiée
		 * @param position point position de la fenêtre à l'affichage
		 */
		public Liste(Point position){
			this(new ResultSetTableModelFactory(EmploiMain.db));

			if(position==null) position = MainGui.centerOnDesktop(getSize());
			setLocation(position);
			setVisible(true);
			MainGui.desktop.add(this);	
			try {
				setMaximum(true);
			} catch (PropertyVetoException e2) {}
		}
		
		/**
		 * Affiche la liste des films avec le ResultSet associé
		 * @param f ResultSetTableModelFactory à afficher
		 * 
		 * @see ResultSetTableModelFactory
		 */
		public Liste(ResultSetTableModelFactory f) {
			setIconifiable(false);
			setClosable(true);
			setResizable(true);
			setMaximizable(true);
			setName(LIST_NAME);
			setSize(550, 450);

			putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);

			recherche = new Recherche();
			
			FormPanel pane = new FormPanel( "be/xtnd/emploi/gui/liste_recherches.jfrm" );
			getContentPane().add(pane);
			
			titre = (TitledSeparator)pane.getComponentByName("titre");
			titre.setText(Messages.getString("fenetre.recherches.titres.liste"));
			
			rs_search = pane.getTextField("raison_sociale");
			
			ville_search = pane.getTextField("ville");
			
			metier_search = pane.getComboBox("metiers");
			commons.buildCombo(metier_search, Metier.listing());
			
			etats_search = pane.getComboBox("etats");
			commons.buildCombo(etats_search, Etat.listing());
			
			envoye_search = pane.getCheckBox("est_envoye");
			envoye_search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					datesState(date_debut_search, envoye_search.isSelected());
					datesState(date_fin_search, envoye_search.isSelected());
				}				
			});
			
			modifie_search = pane.getCheckBox("est_modifie");
			modifie_search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					datesState(date_debut_modif, modifie_search.isSelected());
					datesState(date_fin_modif, modifie_search.isSelected());
				}				
			});
			
			/*contacte_search = pane.getCheckBox("non_contacte");
			contacte_search.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					etats_search.setEnabled(!contacte_search.isSelected());
				}				
			});*/
			
			date_debut_search = (JDateChooser)pane.getComponentByName("date_debut");			
			date_debut_search.setDate(current_date);
			date_fin_search = (JDateChooser)pane.getComponentByName("date_fin");
			date_fin_search.setDate(current_date);
			
			datesState(date_debut_search, false);
			datesState(date_fin_search, false);
			
			date_debut_modif = (JDateChooser)pane.getComponentByName("date_debut_modif");			
			date_debut_modif.setDate(current_date);
			date_fin_modif = (JDateChooser)pane.getComponentByName("date_fin_modif");
			date_fin_modif.setDate(current_date);
			
			datesState(date_debut_modif, false);
			datesState(date_fin_modif, false);
			
			search = (JButton)pane.getButton("chercher");
			search.setText(Messages.getString("fenetre.recherches.boutons.chercher"));
			search.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
			        	storeSort = sorter.getSortingColumns();
						displayQueryResults(constructQuery());
					} catch (Exception e1) {e1.printStackTrace();}
				}
			});
			
			reinit = (JButton)pane.getButton("reinit");
			reinit.setText(Messages.getString("fenetre.recherches.boutons.reinitialiser"));
			reinit.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					rs_search.setText("");
					ville_search.setText("");
					metier_search.setSelectedIndex(0);
					etats_search.setSelectedIndex(0);
					etats_search.setEnabled(true);
					envoye_search.setSelected(false);
					modifie_search.setSelected(false);
					/*contacte_search.setSelected(false);*/
					date_debut_search.setDate(current_date);
					date_fin_search.setDate(current_date);
					date_debut_modif.setDate(current_date);
					date_fin_modif.setDate(current_date);
					datesState(date_debut_search, false);
					datesState(date_fin_search, false);
					datesState(date_debut_modif, false);
					datesState(date_fin_modif, false);
					storeSort = null;
					displayQueryResults(Recherche.LIST_QUERY);
				}
			});

			factory = f;
			table = pane.getTable("table");
	    	this.getRootPane().setDefaultButton(search);
			table.getTableHeader().setReorderingAllowed(false);
			table.addMouseListener(new MouseAdapter(){
				public void mousePressed(MouseEvent e){
			  	  	int nModifier = e.getModifiers();
			    	if ((nModifier & InputEvent.BUTTON3_MASK) != 0) {
						id_selected = new Integer(table.getValueAt(table.rowAtPoint(e.getPoint()),0).toString());
						table.changeSelection(table.rowAtPoint(e.getPoint()),0,false,false);
						popup.show((Component)e.getSource(),e.getX(),e.getY());
			      	}
		            if (e.getClickCount() == 2) {// double-click
		            	id_selected = new Integer(table.getValueAt(table.rowAtPoint(e.getPoint()),0).toString());
						if(MainGui.verifUnicite("recherche_"+id_selected,null)){
							modifieRecherche(id_selected);
						}
					}

				}
			});
			displayQueryResults(Recherche.LIST_QUERY);
			
			popup = new JPopupMenu();
			popupEntries(popup, Messages.getString("window.table_popup.new"),"new",Menus.NEW_IMAGE);
			popupEntries(popup, Messages.getString("window.table_popup.modif"),"detailled",Menus.EMPLOI_ICON);
			popup.addSeparator();
			popupEntries(popup, Messages.getString("window.table_popup.del"),"del",Menus.DELETE_IMAGE);

			table.add(popup);
		}
		
		/**
		 * Construit la requête de recherche
		 * 
		 * @return chaîne sql de recherche de film
		 * @throws Exception
		 */
		public String constructQuery() throws Exception{
			boolean search = false;
			StringBuffer sb = new StringBuffer(Recherche.BASE_LIST_QUERY);
			
			if(!rs_search.getText().trim().equals("")){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				sb.append("raison_sociale LIKE '%"+rs_search.getText()+"%'");
			}
			if(!ville_search.getText().trim().equals("")){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				sb.append("ville LIKE '%"+ville_search.getText()+"%'");
			}
			if(metier_search.getSelectedIndex()!=0){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				sb.append(Metier.TABLE+".metier='"+metier_search.getSelectedItem()+"'");
			}
			if(envoye_search.isSelected()){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String str_date_debut = dateFormat.format(date_debut_search.getDate());
				String str_date_fin = dateFormat.format(date_fin_search.getDate());
				sb.append("date_envoi BETWEEN '"+str_date_debut+"' AND '"+str_date_fin+"'");
			}
			if(modifie_search.isSelected()){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String str_date_debut = dateFormat.format(date_debut_modif.getDate());
				String str_date_fin = dateFormat.format(date_fin_modif.getDate());
				sb.append("date_modif BETWEEN '"+str_date_debut+"' AND '"+str_date_fin+"'");
			}
			/*if(contacte_search.isSelected()){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				sb.append("contacte=0");
			}else */
			if(etats_search.getSelectedIndex()!=0){
				sb.append((search)?" AND ":" WHERE ");
				search = true;
				sb.append(Etat.TABLE+".etat='"+Database.replace(etats_search.getSelectedItem().toString(),"\'","\\\'")+"'");
			}
			
			sb.append(Recherche.ORDER_BY);
			
			String qry = new String();
			qry = sb.toString();
			return qry;
		}
		
		/**
		 * Affiche les données d'une requête sql dans une JTable
		 * @param q requête à exécuter
		 */
		public void displayQueryResults(final String q) {
			MainGui.statusBar.setText("Contacting database...");

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						table.setModel(factory.getResultSetTableModel(q));
						
				        sorter = new TableSorter(table.getModel());
				        if(storeSort!=null){
				        	sorter.setSortingColumns(storeSort);
				        }
				        
				        table.setModel(sorter);
				        sorter.setTableHeader(table.getTableHeader());
				        table.getTableHeader().setToolTipText(CommonsI18n.tr("Click to sort on a column, Ctrl+Click to sort on extra column"));
						
				        TableColumnModel columnModel = table.getColumnModel();
						columnModel.getColumn(0).setPreferredWidth(0);
						columnModel.getColumn(0).setMinWidth(0);
						columnModel.getColumn(0).setMaxWidth(0);
						columnModel.getColumn(3).setPreferredWidth(90);
						columnModel.getColumn(3).setMinWidth(90);
						columnModel.getColumn(3).setMaxWidth(90);
						columnModel.getColumn(3).setCellRenderer(new DateRenderer());
						columnModel.getColumn(4).setPreferredWidth(90);
						columnModel.getColumn(4).setMinWidth(90);
						columnModel.getColumn(4).setMaxWidth(90);
						columnModel.getColumn(4).setCellRenderer(new DateRenderer());

						titre.setText(Messages.getString("fenetre.recherches.titres.liste")+" ("+table.getRowCount()+")");

						MainGui.verifUnicite(IFRecherche.LIST_NAME, null);
						MainGui.statusBar.setText("");
					} catch (SQLException ex) {
						MainGui.statusBar.setText("Error contacting database !");
						JOptionPane.showMessageDialog(Liste.this,
								new String[] { // Display a 2-line message
								ex.getClass().getName() + ": ", ex.getMessage() });
						MainGui.statusBar.setText("");					
					}
				}
			});
		}	
		
		/**
		 * Affiche la fenêtre d'ajout de recherche
		 * Utile pour appels depuis une classe 
		 * externe (notamment pour le raccourci d'ajout
		 * de recherche)
		 */
		public void ajoute(){
			nouvelleRecherche();
		}
		
		@Override
		protected void popupActions(ActionEvent event){
			String command = event.getActionCommand();
			if(command.equals("new")){
				if(MainGui.verifUnicite(NEW_NAME,null)){
					nouvelleRecherche();
				}
			}else if(command.equals("del")){
				supprimeRecherche(id_selected);
			}else if(command.equals("detailled")){
				if(MainGui.verifUnicite("recherche_"+id_selected,null)){
					modifieRecherche(id_selected);
				}
			}
		}	
	}

	class DateRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 1945667515186318801L;
			/**
			 * 
			 *
			 */
			public DateRenderer() {
				super();
			}
			
			/**
			 * @param table
			 * @param value
			 * @param isSelected
			 * @param hasFocus
			 * @param row
			 * @param column
			 * 
			 * @return Component
			 */
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
				JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
	            cell.setHorizontalAlignment(CENTER); 
	            DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
	            String str = new String(); 
				if(!(value == null)){
		            str = df.format(value);
	            }
	            cell.setText(str);

	            return cell;
		  }
	}

	class DateTimeRenderer extends DefaultTableCellRenderer {
		private static final long serialVersionUID = 1945667515186318801L;
			/**
			 * 
			 *
			 */
			public DateTimeRenderer() {
				super();
			}
			
			/**
			 * @param table
			 * @param value
			 * @param isSelected
			 * @param hasFocus
			 * @param row
			 * @param column
			 * 
			 * @return Component
			 */
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
		  		boolean hasFocus, int row, int column) {
				JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, 
	        		isSelected, hasFocus, row, column);
	            cell.setHorizontalAlignment(CENTER);
	    		DateFormat df = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
	            String str = new String(); 
				if(!(value == null)){
		            str = df.format(value);
	            }
	            cell.setText(str);

	            return cell;
		  }
	}


}
