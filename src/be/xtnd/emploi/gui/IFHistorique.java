/*
 * IFHistorique.java, 17 février 2009
 * 
 * This file is part of emplois.
 *
 * Copyright © 2009 Johan Cwiklinski
 *
 * File :               IFHistoriqueEtat.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.WindowConstants;

import be.xtnd.commons.GuiCommons;
import be.xtnd.commons.MessagesCommons;
import be.xtnd.commons.db.Database;
import be.xtnd.commons.gui.EscapeInternalFrame;
import be.xtnd.commons.gui.MainGui;
import be.xtnd.commons.i18n.CommonsI18n;
import be.xtnd.emploi.Messages;
import be.xtnd.emploi.objects.Etat;
import be.xtnd.emploi.objects.Historique;
import be.xtnd.emploi.objects.Recherche;

import com.jeta.forms.components.panel.FormPanel;
import com.jeta.forms.components.separator.TitledSeparator;
import com.jgoodies.looks.plastic.PlasticInternalFrameUI;
import com.toedter.calendar.JDateChooser;
import com.toedter.components.JSpinField;

/**
 * @author Johan Cwiklinski
 *
 */
public class IFHistorique {
	private Historique historique;
	private GuiCommons commons = new GuiCommons();
	/** Nom de la fenêtre liste */
	/*public static final String LIST_NAME = "liste_historique";*/
	/** Nom de la fenêtre d'ajout */
	public static final String NEW_NAME = "nouvel_historique";
	private char mode;
	//private JTable table;
	//private ResultSetTableModelFactory factory; 
	//private Liste liste;
	private DetailWindow details;
	private JComboBox field_etat;
	private JDateChooser field_date;
	private JSpinField field_heure;
	private JSpinField field_minutes;	
	private JTextArea field_commentaire;

	/**
	 * Affiche la liste
	 */
	/*public void getList(){
		liste = new Liste();
	}*/

	/**
	 * Affiche la fenêtre d'ajout d'état
	 * @param id_recherche
	 */
	public void nouvelHistorique(Historique historique){
		/*historique = new Historique();
		historique.setIdRecherche(id_recherche);*/
		this.historique = historique;
		mode = Database.NEW;
		details = new DetailWindow(historique.getIdRecherche());
	}
	
	/**
	 * Affiche la fenêtre de modification de l'entrée d'historique sélectionnée
	 * @param id identifiant de l'entrée à modifier
	 */
	public void modifieHistorique(Integer id){
		historique = new Historique(id);
		mode = Database.MODIF;
		details = new DetailWindow(historique.getIdRecherche());
	}

	/**
	 * Supprimer une entrée d'historique 
	 * @param id identifiant de l'entrée à supprimer
	 */
	public void supprimeHistorique(Integer id){
		historique = new Historique(id);
		Object[] args = {new Recherche(historique.getIdRecherche()).getRaison_sociale()};
		int result = 0;
		int response = JOptionPane.showConfirmDialog(
				MainGui.desktop, 
				MessageFormat.format(Messages.getString("fenetre.historique.messages.suppression.confirmation"), args),
				Messages.getString("fenetre.historique.messages.suppression.titre"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE);

		if (response == JOptionPane.YES_OPTION) {
			result = historique.delete(id);		

			String strMessage = new String();
			int typeMessage = 0;
			String titreMessage = new String();
			
			if(result>0){
				strMessage = MessageFormat.format(
						Messages.getString("fenetre.historique.messages.suppression"),
						args);
				titreMessage = Messages.getString("fenetre.historique.messages.suppression.titre");
				typeMessage = JOptionPane.INFORMATION_MESSAGE;
			}else{
				if(result==-2){
					strMessage = MessageFormat.format(
							Messages.getString("fenetre.historique.messages.fk"),
							args);
					titreMessage = Messages.getString("fenetre.historique.messages.fk.titre");
					typeMessage = JOptionPane.WARNING_MESSAGE;					
				}else{
					strMessage = MessageFormat.format(
							Messages.getString("fenetre.historique.messages.suppression.echec"),
							args);
					titreMessage = CommonsI18n.tr("Error!");
					typeMessage = JOptionPane.ERROR_MESSAGE;
				}
			}
			JOptionPane.showMessageDialog(
					MainGui.desktop, 
					strMessage,
					titreMessage,
					typeMessage);
			try {
				if(!MainGui.verifUnicite("recherche_" + historique.getIdRecherche(), null)){
					IFRecherche.DetailWindow liste = null;
					JInternalFrame[] frames = MainGui.desktop.getAllFrames();
					int i = 0;
					while(i < frames.length){
						if(frames[i].getName().equals("recherche_" + historique.getIdRecherche())){
							liste = (IFRecherche.DetailWindow)frames[i];
							break;
						}
						i++;
					}
					liste.displayQueryResults(Historique.getListQuery(historique.getIdRecherche()));
				}
			} catch (Exception e1) {e1.printStackTrace();}
		}
		
	}

	/**
	 * Vérifie les champs obligatoires
	 * @return true si les entrées sont valides, false sinon
	 */
	protected boolean verifEntrees(){
		boolean valid = true;
		StringBuffer sb = new StringBuffer();
		sb.append(MessagesCommons.getString("window.messages.required"));

		if(field_etat.getSelectedIndex() == 0){
			sb.append("\n- "+Messages.getString("fenetre.historique.messages.requis.etat"));
			if(valid) field_etat.requestFocus();
			valid = false;
		}

		if(!valid){
			JOptionPane.showMessageDialog(
				MainGui.desktop, 
				sb.toString(),
				CommonsI18n.tr("Error!"),
				JOptionPane.ERROR_MESSAGE);			
		}
		return valid;
	}

	/**
	 * Enregistre les données de l'entrée d'historique
	 *
	 */
	protected void queries(){
		boolean nouveau = false;
		String messageTitre = new String();
		int boite = 0;
		String message = new String();
		
		historique.setEtat(new Etat((String)field_etat.getSelectedItem()));
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(field_date.getDate());
		
		cal.set(Calendar.HOUR, field_heure.getValue());
		cal.set(Calendar.MINUTE, field_minutes.getValue());
		
		historique.setDate(cal.getTime());
		historique.setCommentaire(field_commentaire.getText());
		
		if(mode==Database.NEW){
			messageTitre = Messages.getString("fenetre.historique.messages.ajout.titre");
			nouveau = true;
		}else if(mode==Database.MODIF){
			messageTitre = Messages.getString("fenetre.historique.messages.modification.titre");
			nouveau = false;
		}
		
		historique.enregistrer(nouveau);

		Object[] args = {new Recherche(historique.getIdRecherche()).getRaison_sociale()};
		if(historique.getAffectedRows()>0){
			boite = JOptionPane.INFORMATION_MESSAGE+JOptionPane.OK_OPTION;
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.historique.messages.ajout"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.historique.messages.modification"), args);
			}
			details.dispose();
			try {
				if(!MainGui.verifUnicite("recherche_" + historique.getIdRecherche(), null)){
					IFRecherche.DetailWindow liste = null;
					JInternalFrame[] frames = MainGui.desktop.getAllFrames();
					int i = 0;
					while(i < frames.length){
						if(frames[i].getName().equals("recherche_" + historique.getIdRecherche())){
							liste = (IFRecherche.DetailWindow)frames[i];
							break;
						}
						i++;
					}
					liste.displayQueryResults(Historique.getListQuery(historique.getIdRecherche()));
				}
			} catch (Exception e1) {e1.printStackTrace();}
		}else{
			boite = JOptionPane.WARNING_MESSAGE+JOptionPane.OK_OPTION;
			if(historique.getSqlMessage().equals("")) historique.setSqlMessage(MessagesCommons.getString("window.messages.noerror"));
			if(mode==Database.NEW){
				message = MessageFormat.format(Messages.getString("fenetre.historique.messages.ajout.echec"), args);
			}else if(mode==Database.MODIF){
				message = MessageFormat.format(Messages.getString("fenetre.historique.messages.modification.echec"), args);
			}
			message = message + "\n("+historique.getSqlMessage()+")";
		}
		JOptionPane.showMessageDialog(MainGui.desktop, message, messageTitre, boite);
	}	

	protected class DetailWindow extends EscapeInternalFrame{
		private static final long serialVersionUID = -3516085133189643664L;
		private TitledSeparator titre;
		
		protected DetailWindow(int id_recherche){
			super();
            putClientProperty(
                    PlasticInternalFrameUI.IS_PALETTE,
                    Boolean.TRUE);
			fenetreChamps();
		}
					
		void fenetreChamps(){
			FormPanel pane = new FormPanel( "be/xtnd/emploi/gui/historique.jfrm" );
			
			titre = (TitledSeparator)pane.getComponentByName("titre");
			
			if(mode==Database.NEW){
				titre.setText(Messages.getString("fenetre.historique.titres.nouveau"));
				setName(NEW_NAME);
			}else{
				Object[] args = {new Recherche(historique.getIdRecherche()).getRaison_sociale() ,historique.getId()};
				String strtitre = MessageFormat.format(Messages.getString("fenetre.historique.titres.modifier"), args);
				titre.setText(strtitre);
				setName("historique_"+historique.getIdRecherche()+"_"+historique.getId());				
			}

			field_etat = pane.getComboBox("etat");
			commons.buildCombo(field_etat, Etat.listing());
			if( historique.getEtat() != null )
				field_etat.setSelectedItem(historique.getEtat().getEtat());
			
			field_date = (JDateChooser) pane.getComponentByName("date");
			field_date.setDateFormatString(Messages.getString("emploi.date.format"));
			field_heure = (JSpinField) pane.getComponentByName("heures");
			field_minutes = (JSpinField) pane.getComponentByName("minutes");
			if( historique.getDate() != null ){
				field_date.setDate(historique.getDate());
			
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(historique.getDate());
			
				field_heure.setValue(cal.get(Calendar.HOUR));
			
				field_minutes.setValue(cal.get(Calendar.MINUTE));
			}
			
			field_commentaire = (JTextArea)pane.getComponentByName("commentaire");
			field_commentaire.setText(historique.getCommentaire());

			//désactivation des champs en lecture seule
			datesState(field_date, false);
			field_etat.setEnabled(false);
			field_heure.setEnabled(false);
			field_minutes.setEnabled(false);
			
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setClosable(true);
			setMaximizable(true);
			setIconifiable(true);
			
			add(pane);
			
			commons.createButtonBar(this,new OkayEvent());
			
			pack();
			setVisible(true);
			setLocation(MainGui.centerOnDesktop(getSize()));
			MainGui.desktop.add(this,JLayeredPane.MODAL_LAYER);
			try {
				setSelected(true);
			} catch (PropertyVetoException e) {}			
		}

		/**
		 * (Dés)active les champs JDateChooser
		 * @param elt élément JDateChooser à modifier
		 * @param tostate état 
		 */
		private void datesState(JDateChooser elt, boolean tostate){
			elt.setEnabled(false);
			Component[] begin_comps = elt.getComponents();
			for (Component comp : begin_comps) {
				comp.setEnabled(tostate);
			}
		}

		class OkayEvent implements ActionListener{
			/**
			 * @param e ActionEvent
			 */
			public void actionPerformed(ActionEvent e) {
				if(verifEntrees())
					queries();
			}		
		}
	}
}
