/*
 * Historique.java, 17 février 2009
 * 
 * This file is part of emplois.
 *
 * Copyright © 2009 Johan Cwiklinski
 *
 * File :               Historique.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.db.Database;
import be.xtnd.emploi.EmploiMain;


/**
 * @author Johan Cwiklinski
 *
 */
public class Historique {
	private static Database database;
	private Integer id, id_recherche;
	private Date date;
	private Etat etat;
	private String commentaire;
	private Recherche recherche;
	/** Nom de la table */
	public static String TABLE = "historique_recherches";
	/** Nom du champ de clé primaire */
	public static String PK = "id_historique";
	/** Requête de liste des éléments */
	public static String LIST_QUERY = "SELECT " + PK + ", " + Etat.TABLE + ".etat AS \"État\", date AS \"Date\", commentaires as \"Commentaires\" FROM " + TABLE + " INNER JOIN " + Etat.TABLE + " ON (" + TABLE + "." + Etat.PK + "=" + Etat.TABLE + "." + Etat.PK + ")";
	/** */
	public static String LIST_ORDER = " ORDER BY date DESC";
	/** valeur par défaut pour l'enregistrement */
	public static int DEFAULT = 1;
	/* variables bdd */
	private static String sqlMessage;
	private static int sqlError = 0;
	private int affectedRows;
	private static Logger logger = Logger.getLogger(Etat.class.getName());

	/**
	 * 
	 *
	 */
	public Historique(){
		database = EmploiMain.db;
		this.date = new Date();
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
	}
	
	/**
	 * 
	 * @param id
	 */
	public Historique(Integer id){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromId(id);
	}

	/**
	 * Initialise un objet joint en fonction de son id
	 * @param id id de l'objet à initialiser
	 */
	public void loadFromId(Integer id) {
		this.id = id;
		try {
			String query = "SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
			ResultSet rs = database.execQuery(query);
			rs.first();
			this.id_recherche = rs.getInt(Recherche.PK);
			this.date = rs.getDate("date");
			this.commentaire = rs.getString("commentaires");
			this.etat = new Etat(rs.getInt("id_etat"));
		} catch (SQLException e) {
			e.printStackTrace();
			logger.fatal("SQLException initializing historique by id : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}	

	/**
	 * Supprime l'objet 
	 * @param id identifiant de l'objet à supprimer
	 * @return entier nombre de lignes affectées 
	 */
	public int delete(Integer id){
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE+" WHERE "+PK+"="+id);
		} catch (SQLException e) {
			if(e.getErrorCode()==1217 || e.getErrorCode()==1451) return -2; //mysql fk fails --> 1217 = mysql 4.1 1451 = mysql 5
			logger.fatal("SQLException deleting historique : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}
	
	/**
	 * Supprime l'objet 
	 * @param id identifiant de la recherche
	 * @return entier nombre de lignes affectées 
	 */
	public static int empty(Integer id){
		database = EmploiMain.db;
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE+" WHERE "+Recherche.PK+"="+id);
		} catch (SQLException e) {
			if(e.getErrorCode()==1217 || e.getErrorCode()==1451) return -2; //mysql fk fails --> 1217 = mysql 4.1 1451 = mysql 5
			logger.fatal("SQLException deleting historique : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}

	/**
	 * Enregistre un objet 
	 * @param nouveau true s'il s'agit d'un nouvel enregistrement,
	 * false s'il sagit d'une modification 
	 */
	public void enregistrer(boolean nouveau){
		String query = new String();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str_date = dateFormat.format(this.date);

		if(nouveau){
			query = "INSERT INTO " + TABLE + " SET id_recherche=" + this.id_recherche + ", id_etat='" + this.etat.getId() + "', " + 
				"date='" + str_date + "', commentaires='" + Database.replace(this.commentaire,"\'","\\\'") + "'";
		}else{
			query = "UPDATE " + TABLE + " SET id_recherche='" + this.id_recherche + "', id_etat='" + this.etat.getId() + "', "+
				"date='" + str_date + "', commentaires='"+Database.replace(this.commentaire,"\'","\\\'")+"'"+
				" WHERE "+PK+"="+this.id;
		}

		//Mise à jour de la date de modification de la recherche
		this.recherche.setDate_modif(this.date);
		System.out.println("Updating recherche: " + str_date);
		
		try {
			affectedRows = database.execUpdate(query);
			if(nouveau){
				ResultSet rs = database.execQuery("SELECT LAST_INSERT_ID() as id");
				while(rs.next())
					this.id=rs.getInt("id");	
			}
			this.recherche.enregistrer(false);
		} catch (SQLException e) {
			logger.fatal("SQLException saving historique (new="+nouveau+") : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}


	/**
	 * Liste des recherches qui utilisent l'état
	 * @return nombre de recherches affectées
	 */
	public int fkRecherches(){
		int compte = 0;
		try {
			String query = "SELECT count("+Recherche.PK+") AS compte FROM "+Recherche.TABLE+" INNER JOIN "
					+TABLE+" ON("+Recherche.TABLE+"."+Recherche.PK+"="+TABLE+"."+PK+")"+
					" WHERE "+TABLE+"."+PK+"="+this.id;
			ResultSet rs = database.execQuery(query);
			compte = rs.getInt("compte");
		} catch (SQLException e) {
			logger.fatal("SQLException getting dependances historique : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}		
		return compte;
	}

	/**
	 * @return nombre d'entrées d'historiqe dans la base
	 */
	public static Integer getCount(Integer id_recherche){
		database = EmploiMain.db;
		int count = -1;
		try{
			String query = "SELECT count(" + PK + ") as compte FROM " + TABLE + " WHERE " + Recherche.PK + "=" + id_recherche;
			ResultSet rs = database.execQuery(query);
			rs.first();
			count = rs.getInt("compte");
		}catch(SQLException e){
			logger.fatal("SQLException counting historiques : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return count;
	}
	
	public static String getListQuery(int id){
		return LIST_QUERY + " WHERE " + Recherche.PK + "=" + id + LIST_ORDER;
	}
	
	/**
	 * @return Renvoie sqlMessage.
	 */
	public String getSqlMessage() {
		return sqlMessage;
	}

	/**
	 * @param message sqlMessage à définir.
	 */
	public void setSqlMessage(String message) {
		sqlMessage = message;
	}

	/**
	 * @return Renvoie id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id à définir.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Renvoie état.
	 */
	public Etat getEtat() {
		return etat;
	}

	/**
	 * @return Renvoie sqlError.
	 */
	public static int getSqlError() {
		return sqlError;
	}

	/**
	 * @return Renvoie affectedRows.
	 */
	public int getAffectedRows() {
		return affectedRows;
	}

	/**
	 * @return Renvoie commentaire.
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @return Renvoie date.
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getIdRecherche(){
		return this.id_recherche;
	}
	
	/**
	 * @param commentaire commentaire à définir.
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @param etat etat à définir.
	 */
	public void setEtat(Etat etat) {
		this.etat = etat;
	}

	/**
	 * @param date date à définir.
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setIdRecherche(int id){
		this.id_recherche = id;
		this.recherche = new Recherche(this.id_recherche);
	}
}
