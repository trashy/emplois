/*
 * Etat.java, 7 mars 2006
 * 
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               Etat.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.db.Database;
import be.xtnd.emploi.EmploiMain;


/**
 * @author Johan Cwiklinski
 *
 */
public class Etat implements Comparable<Etat> {
	private static Database database;
	private Integer id;
	private String etat;
	private String commentaire;
	/** Nom de la table */
	public static String TABLE = "etat";
	/** Nom du champ de clé primaire */
	public static String PK = "id_etat";
	/** Requête de liste des éléments */
	public static String LIST_QUERY = "SELECT "+PK+", etat AS \"État\", commentaire FROM "+TABLE+" ORDER BY etat ASC";
	/** valeur par défaut pour l'enregistrement */
	public static int DEFAULT = 1;
	/* variables bdd */
	private static String sqlMessage;
	private static int sqlError = 0;
	private int affectedRows;
	private static Logger logger = Logger.getLogger(Etat.class.getName());

	/**
	 * 
	 *
	 */
	public Etat(){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
	}
	
	/**
	 * 
	 * @param id
	 */
	public Etat(Integer id){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromId(id);
	}

	/**
	 * 
	 * @param etat
	 */
	public Etat(String etat){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromValue(etat);
	}

	/**
	 * Initialise un objet joint en fonction de son id
	 * @param id id de l'objet à initialiser
	 */
	public void loadFromId(Integer id) {
		this.id = id;
		try {
			String query = "SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
			ResultSet rs = database.execQuery(query);
			rs.first();
				this.etat = rs.getString("etat");
				this.commentaire = rs.getString("commentaire");
		} catch (SQLException e) {
			e.printStackTrace();
			logger.fatal("SQLException initializing etat by id : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}	

	/**
	 * Initialise un objet en fonction de sa valeur
	 * @param valeur valeur de l'objet à initialiser
	 */
	public void loadFromValue(String valeur) {
		this.etat = valeur;
		try {
			ResultSet rs = database.execQuery(
					"SELECT * FROM "+TABLE+" WHERE etat='"+
					Database.replace(valeur,"\'","\\\'")+"'");
			while(rs.next()){
				this.id = rs.getInt(PK);
				this.commentaire = rs.getString("commentaire");
			}
		} catch (SQLException e) {
			logger.fatal("SQLException initializing état by name : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}
	
	/**
	 * Supprime l'objet 
	 * @param id identifiant de l'objet à supprimer
	 * @return entier nombre de lignes affectées 
	 */
	public int delete(Integer id){
		// on ne supprime pas l'état par défaut !!
		if( id == Etat.DEFAULT) return -10;
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE+" WHERE "+PK+"="+id);
		} catch (SQLException e) {
			if(e.getErrorCode()==1217 || e.getErrorCode()==1451) return -2; //mysql fk fails --> 1217 = mysql 4.1 1451 = mysql 5
			logger.fatal("SQLException deleting état : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}
	
	/**
	 * Enregistre un objet 
	 * @param nouveau true s'il s'agit d'un nouvel enregistrement,
	 * false s'il sagit d'une modification 
	 */
	public void enregistrer(boolean nouveau){
		String query = new String();
		if(nouveau){
			query = "INSERT INTO "+TABLE+" SET etat='"+
				Database.replace(this.etat,"\'","\\\'")+"', "+
				"commentaire='"+Database.replace(this.commentaire,"\'","\\\'")+"'";
		}else{
			query = "UPDATE "+TABLE+" SET etat='"+
				Database.replace(this.etat,"\'","\\\'")+"', "+
				"commentaire='"+Database.replace(this.commentaire,"\'","\\\'")+"'"+
				" WHERE "+PK+"="+this.id;
		}
		try {
			affectedRows = database.execUpdate(query);
			if(nouveau){
				ResultSet rs = database.execQuery("SELECT LAST_INSERT_ID() as id");
				while(rs.next())
					this.id=rs.getInt("id");	
			}
		} catch (SQLException e) {
			logger.fatal("SQLException saving état (new="+nouveau+") : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}


	/**
	 * Liste des recherches qui utilisent l'état
	 * @return nombre de recherches affectées
	 */
	public int fkRecherches(){
		int compte = 0;
		try {
			String query = "SELECT count("+Recherche.PK+") AS compte FROM "+Recherche.TABLE+" INNER JOIN "
					+TABLE+" ON("+Recherche.TABLE+"."+Recherche.PK+"="+TABLE+"."+PK+")"+
					" WHERE "+TABLE+"."+PK+"="+this.id;
			ResultSet rs = database.execQuery(query);
			compte = rs.getInt("compte");
		} catch (SQLException e) {
			logger.fatal("SQLException getting dependances état : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}		
		return compte;
	}

	/**
	 * Liste des objets joins par ordre alphabétique
	 * @return ArrayList des objets
	 */
	public static ArrayList<String> listing(){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));

		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT etat FROM "+TABLE+" ORDER BY etat ASC";
		try{
			ResultSet rs = database.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("etat"));
			}
		}catch(SQLException e){
			logger.fatal("SQLException listing états : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		list.trimToSize();
		return list;
	}
		
	/**
	 * @return nombre d'acteurs dans la base
	 */
	public Integer getCount(){
		int count = -1;
		try{
			String query = "SELECT count("+PK+") as compte FROM "+TABLE;
			ResultSet rs = database.execQuery(query);
			rs.first();
			count = rs.getInt("compte");
		}catch(SQLException e){
			logger.fatal("SQLException counting états : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return count;
	}	
	/**
	 * @return Renvoie sqlMessage.
	 */
	public String getSqlMessage() {
		return sqlMessage;
	}

	/**
	 * @param message sqlMessage à définir.
	 */
	public void setSqlMessage(String message) {
		sqlMessage = message;
	}

	/**
	 * @return Renvoie id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id à définir.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Renvoie état.
	 */
	public String getEtat() {
		return etat;
	}

	/**
	 * @return Renvoie sqlError.
	 */
	public static int getSqlError() {
		return sqlError;
	}

	/**
	 * @return Renvoie affectedRows.
	 */
	public int getAffectedRows() {
		return affectedRows;
	}

	/**
	 * @return Renvoie commentaire.
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire commentaire à définir.
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	/**
	 * @param etat etat à définir.
	 */
	public void setEtat(String etat) {
		this.etat = etat;
	}


	@Override
	public int compareTo(Etat o) {
		if( this.id == o.id && this.etat.equals(o.etat) && this.commentaire.equals(o.commentaire) ){
			return 0;
		}
		return -1;
	}

}
