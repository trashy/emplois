/*
 * Recherche.java, 7 mars 2006
 * 
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               Recherche.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.db.Database;
import be.xtnd.emploi.EmploiMain;


/**
 * @author Johan Cwiklinski
 *
 */
public class Recherche {
	private static Database database;
	private Integer id;
	private String raison_sociale;
	private String adresse;
	private String cp;
	private String ville;
	private String telephone;
	private String mail;
	private String site;
	private String contact;
	private String contact_poste;
	private String commentaires;
	private boolean envoye;
	private Date date_creation;
	private Date date_modif;
	private Date date_rappel;
	/*private boolean contacte;*/
	private Metier metier;
	private Etat etat;
	private boolean has_changed;
	/** Nom de la table */
	public static String TABLE = "recherches";
	/** Nom du champ de clé primaire */
	public static String PK = "id_recherche";
	/** Requête de liste des recherches non ordonnée */
	public static String BASE_LIST_QUERY = "SELECT "+TABLE+"."+PK+", "+TABLE+".raison_sociale AS \"Raison Sociale\", "+
		TABLE+".ville AS \"Ville\", "+TABLE+".date_modif AS \"Modifé\", "+TABLE+".date_rappel AS \"Rappel\", "+Metier.TABLE+".metier AS \"Corps métier\", "+Etat.TABLE+".etat AS \"État\""+
		" FROM "+TABLE+" INNER JOIN "+Metier.TABLE+
		" ON ("+Metier.TABLE+"."+Metier.PK+"="+TABLE+"."+Metier.PK+") INNER JOIN "+Etat.TABLE+
		" ON ("+Etat.TABLE+"."+Etat.PK+"="+TABLE+"."+Etat.PK+")";
	/** Clause Orderby */
	public static String ORDER_BY = " ORDER BY "+TABLE+".date_modif DESC";
	/** Requête de liste des recherches */
	public static String LIST_QUERY = BASE_LIST_QUERY+ORDER_BY;
	/* variables bdd */
	private static String sqlMessage;
	private static int sqlError = 0;
	private int affectedRows;
	private static Logger logger = Logger.getLogger(Recherche.class.getName());

	/**
	 * 
	 *
	 */
	public Recherche(){
		database = EmploiMain.db;
		this.metier = new Metier();
		this.etat = new Etat();
		this.telephone = "";
		this.date_creation = Calendar.getInstance().getTime();
		this.date_modif = Calendar.getInstance().getTime();
		this.date_rappel = Calendar.getInstance().getTime();
		this.has_changed = false;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
	}
	
	/**
	 * 
	 * @param id
	 */
	public Recherche(Integer id){
		database = EmploiMain.db;
		this.metier = new Metier();
		this.etat = new Etat();
		this.has_changed = false;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromId(id);
	}
	
	/**
	 * Initialise un objet joint en fonction de son id
	 * @param id id de l'objet à initialiser
	 */
	public void loadFromId(Integer id) {
		this.id = id;
		try {
			String query = "SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
			ResultSet rs = database.execQuery(query);
			rs.first();
				this.raison_sociale = rs.getString("raison_sociale");
				this.adresse = rs.getString("adresse");
				this.ville = rs.getString("ville");
				this.cp = rs.getString("cp");
				this.telephone = rs.getString("telephone");
				this.mail = rs.getString("mail");
				this.site = rs.getString("site");
				this.contact = rs.getString("contact");
				this.contact_poste = rs.getString("poste_contact");
				this.commentaires = rs.getString("commentaires");
				this.envoye = rs.getBoolean("envoye");
				this.date_creation = rs.getDate("date_creation");
				this.date_modif = rs.getDate("date_modif");
				this.date_rappel = rs.getDate("date_rappel");
				//this.contacte = rs.getBoolean("contacte");
				
				int loadMetier = rs.getInt(Metier.PK);
				int loadEtat = rs.getInt(Etat.PK);
				
				metier.loadFromId(loadMetier);
				etat.loadFromId(loadEtat);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.fatal("SQLException initializing recherche by id : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}	
	
	/**
	 * Supprime l'objet 
	 * @param id identifiant de l'objet à supprimer
	 * @return entier nombre de lignes affectées 
	 */
	public int delete(Integer id){
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE+" WHERE "+PK+"="+id);
		} catch (SQLException e) {
			logger.fatal("SQLException deleting recherche : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}

	/**
	 * Vide la table 
	 * @return entier nombre de lignes affectées 
	 */
	public static int deleteAll(){
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE);
		} catch (SQLException e) {
			logger.fatal("SQLException deleting recherche : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}

	/**
	 * Enregistre un objet 
	 * @param nouveau true s'il s'agit d'un nouvel enregistrement,
	 * false s'il sagit d'une modification 
	 */
	public void enregistrer(boolean nouveau){
		String query = new String();

		if(nouveau){
			query = "INSERT INTO " + TABLE + buidQueries();
		}else{
			query = "UPDATE " + TABLE + buidQueries() + " WHERE "+PK+"="+this.id;
		}
		System.err.println(query);
		try {
			affectedRows = database.execUpdate(query);
			if(nouveau){
				ResultSet rs = database.execQuery("SELECT LAST_INSERT_ID() as id");
				while(rs.next())
					this.id=rs.getInt("id");	
			}
		} catch (SQLException e) {
			logger.fatal("SQLException saving recherche (new="+nouveau+") : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}
	
	private String buidQueries(){
		StringBuffer sb = new StringBuffer(" SET ");

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String str_date_creation = (this.date_creation!=null)?dateFormat.format(this.date_creation):"0000-00-00";
		String str_date_modification = (this.date_modif!=null)?dateFormat.format(this.date_modif):"0000-00-00";
		String str_date_rappel = (this.date_rappel!=null)?dateFormat.format(this.date_rappel):"0000-00-00";

		sb.append("raison_sociale='"+Database.replace(this.raison_sociale,"\'","\\\'")+"', ");
		sb.append("adresse='"+Database.replace(this.adresse,"\'","\\\'")+"', ");
		sb.append("cp='"+Database.replace(this.cp,"\'","\\\'")+"', " );
		sb.append("ville='"+Database.replace(this.ville,"\'","\\\'")+"', ");
		sb.append("telephone='"+Database.replace(this.telephone, " ", null)+"', ");
		sb.append("mail='"+Database.replace(this.mail,"\'","\\\'")+"', ");
		sb.append("site='"+Database.replace(this.site,"\'","\\\'")+"', ");
		sb.append("contact='"+Database.replace(this.contact,"\'","\\\'")+"', ");
		sb.append("poste_contact='"+Database.replace(this.contact_poste,"\'","\\\'")+"', ");
		sb.append("commentaires='"+Database.replace(this.commentaires,"\'","\\\'")+"', ");
		sb.append("envoye="+this.envoye+", ");
		sb.append("date_creation='"+str_date_creation+"', ");
		sb.append("date_modif='"+str_date_modification+"', ");
		sb.append("date_rappel='"+str_date_rappel+"', ");
		//sb.append("contacte="+this.contacte+", ");
		sb.append(Metier.PK+"="+this.metier.getId()+", ");
		sb.append(Etat.PK+"="+this.etat.getId());

		return sb.toString();
	}
		
	/**
	 * @return nombre de recherches dans la base
	 */
	public Integer getCount(){
		int count = -1;
		try{
			String query = "SELECT count("+PK+") as compte FROM "+TABLE;
			ResultSet rs = database.execQuery(query);
			rs.first();
			count = rs.getInt("compte");
		}catch(SQLException e){
			logger.fatal("SQLException counting recherches : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return count;
	}	
	/**
	 * @return Renvoie sqlMessage.
	 */
	public String getSqlMessage() {
		return sqlMessage;
	}

	/**
	 * @param message sqlMessage à définir.
	 */
	public void setSqlMessage(String message) {
		sqlMessage = message;
	}

	/**
	 * @return Renvoie id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id à définir.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Renvoie sqlError.
	 */
	public static int getSqlError() {
		return sqlError;
	}

	/**
	 * @return Renvoie affectedRows.
	 */
	public int getAffectedRows() {
		return affectedRows;
	}

	/**
	 * @return Renvoie adresse.
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse adresse à définir.
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return Renvoie commentaires.
	 */
	public String getCommentaires() {
		return commentaires;
	}

	/**
	 * @param commentaires commentaires à définir.
	 */
	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

	/**
	 * @return Renvoie contact.
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact contact à définir.
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return Renvoie le poste du contact
	 */
	public String getPosteContact(){
		return contact_poste;
	}
	
	/**
	 * @param poste poste à définir
	 */
	public void setPosteContact(String poste){
		this.contact_poste = poste;
	}
	
	/**
	 * @return Renvoie date_creation.
	 */
	public Date getDate_creation() {
		return date_creation;
	}

	/**
	 * @param date_creation à définir.
	 */
	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}

	/**
	 * @return Renvoie date_rappel.
	 */
	public Date getDate_rappel() {
		return date_rappel;
	}

	/**
	 * @param date_rappel date_rappel à définir.
	 */
	public void setDate_rappel(Date date_rappel) {
		this.date_rappel = date_rappel;
	}

	/**
	 * @return Renvoie metier.
	 */
	public Metier getMetier() {
		return metier;
	}

	/**
	 * @param metier metier à définir.
	 */
	public void setMetier(Metier metier) {
		this.metier = metier;
	}

	/**
	 * @return Renvoie raison_sociale.
	 */
	public String getRaison_sociale() {
		return raison_sociale;
	}

	/**
	 * @param raison_sociale raison_sociale à définir.
	 */
	public void setRaison_sociale(String raison_sociale) {
		this.raison_sociale = raison_sociale;
	}

	/**
	 * @return Renvoie telephone.
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone telephone à définir.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return Renvoie ville.
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville ville à définir.
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}

	/**
	 * @return Renvoie etat.
	 */
	public Etat getEtat() {
		return etat;
	}

	/**
	 * @param etat etat à définir.
	 */
	public void setEtat(Etat etat) {
		System.err.println("comparing state: " + this.etat.compareTo(etat) );
		if( this.etat.compareTo(etat) != 0 ) this.has_changed = true;
		this.etat = etat;
	}

	/**
	 * @return Renvoie contacte.
	 */
	/*public boolean isContacte() {
		return contacte;
	}*/

	/**
	 * @param contacte contacte à définir.
	 */
	/*public void setContacte(boolean contacte) {
		this.contacte = contacte;
	}*/

	/**
	 * @return Renvoie cp.
	 */
	public String getCp() {
		return cp;
	}

	/**
	 * @param cp cp à définir.
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}

	/**
	 * @return Renvoie envoye.
	 */
	public boolean isEnvoye() {
		return envoye;
	}

	/**
	 * @param envoye envoye à définir.
	 */
	public void setEnvoye(boolean envoye) {
		this.envoye = envoye;
	}

	/**
	 * @return Renvoie mail.
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail mail à définir.
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return Renvoie site.
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @param site site à définir.
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * 
	 * @return Renvoie la date de modification
	 */
	public Date getDate_modif() {
		return date_modif;
	}

	/**
	 * 
	 * @param date_modif date de modification à définir
	 */
	public void setDate_modif(Date date_modif) {
		System.err.println("comparing dates: " + this.date_modif.compareTo(date_modif) );
		if( this.date_modif.compareTo(date_modif) != 0 ) this.has_changed = true;
		this.date_modif = date_modif;
	}
	
	/**
	 * 
	 * @return L'enregistrement a-t-il été modifié ? Utile pour l'ajout de commentaire sur l'historique
	 */
	public boolean hasChanged(){
		return this.has_changed;
	}
}
