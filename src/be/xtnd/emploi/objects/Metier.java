/*
 * Metier.java, 7 mars 2006
 * 
 * This file is part of emplois.
 *
 * Copyright © 2006 Johan Cwiklinski
 *
 * File :               Metier.java
 * Author's email :     johan@x-tnd.be
 * Author's Website :   http://www.x-tnd.be/
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 */

/**
 * 
 */
package be.xtnd.emploi.objects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import be.xtnd.commons.db.Database;
import be.xtnd.emploi.EmploiMain;


/**
 * @author Johan Cwiklinski
 *
 */
public class Metier {
	private static Database database;
	private Integer id;
	private String metier;
	/** Nom de la table */
	public static String TABLE = "metiers";
	/** Nom du champ de clé primaire */
	public static String PK = "id_metier";
	/** Requête de liste des éléments */
	public static String LIST_QUERY = "SELECT id_metier, metier AS \"Métier\" FROM "+TABLE+" ORDER BY metier ASC";
	/* variables bdd */
	private static String sqlMessage;
	private static int sqlError = 0;
	private int affectedRows;
	private static Logger logger = Logger.getLogger(Metier.class.getName());

	/**
	 * 
	 *
	 */
	public Metier(){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
	}
	
	/**
	 * 
	 * @param id
	 */
	public Metier(Integer id){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromId(id);
	}
	
	/**
	 * 
	 * @param metier
	 */
	public Metier(String metier){
		database = EmploiMain.db;
		PropertyConfigurator.configure(ClassLoader.getSystemResource("be/xtnd/commons/log4j.properties"));
		loadFromValue(metier);		
	}
	
	/**
	 * Initialise un objet joint en fonction de son id
	 * @param id id de l'objet à initialiser
	 */
	public void loadFromId(Integer id) {
		this.id = id;
		try {
			String query = "SELECT * FROM "+TABLE+" WHERE "+PK+"="+id;
			ResultSet rs = database.execQuery(query);
			rs.first();
				this.metier = rs.getString("metier");
		} catch (SQLException e) {
			e.printStackTrace();
			logger.fatal("SQLException initializing metier by id : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}	

	/**
	 * Initialise un objet en fonction de sa valeur
	 * @param valeur valeur de l'objet à initialiser
	 */
	public void loadFromValue(String valeur) {
		this.metier = valeur;
		try {
			ResultSet rs = database.execQuery(
					"SELECT * FROM "+TABLE+" WHERE metier='"+
					Database.replace(valeur,"\'","\\\'")+"'");
			while(rs.next())
				this.id = rs.getInt(PK);
		} catch (SQLException e) {
			logger.fatal("SQLException initializing metier by name : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}
	
	/**
	 * Supprime l'objet 
	 * @param id identifiant de l'objet à supprimer
	 * @return entier nombre de lignes affectées 
	 */
	public int delete(Integer id){
		int result=0;
		try {
			result = database.execUpdate("DELETE FROM "+TABLE+" WHERE "+PK+"="+id);
		} catch (SQLException e) {
			if(e.getErrorCode()==1217 || e.getErrorCode()==1451) return -2; //mysql fk fails
			logger.fatal("SQLException deleting metier : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return result;
	}
	
	/**
	 * Enregistre un objet 
	 * @param nouveau true s'il s'agit d'un nouvel enregistrement,
	 * false s'il sagit d'une modification 
	 */
	public void enregistrer(boolean nouveau){
		String query = new String();
		if(nouveau){
			query = "INSERT INTO "+TABLE+" SET metier='"+
				Database.replace(this.metier,"\'","\\\'")+"'";
		}else{
			query = "UPDATE "+TABLE+" SET metier='"+
				Database.replace(this.metier,"\'","\\\'")+
				"' WHERE "+PK+"="+this.id;
		}
		try {
			affectedRows = database.execUpdate(query);
			if(nouveau){
				ResultSet rs = database.execQuery("SELECT LAST_INSERT_ID() as id");
				while(rs.next())
					this.id=rs.getInt("id");	
			}
		} catch (SQLException e) {
			logger.fatal("SQLException saving metier (new="+nouveau+") : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
	}


	/**
	 * Liste des recherches qui utilisent le métier
	 * @return nombre de recherches affectées
	 */
	public int fkRecherches(){
		int compte = 0;
		try {
			String query = "SELECT COUNT("+PK+") AS compte FROM "+Recherche.TABLE+" INNER JOIN "
					+TABLE+" ON("+Recherche.TABLE+"."+Recherche.PK+"="+TABLE+"."+PK+")"+
					" WHERE "+TABLE+"."+PK+"="+this.id;
			ResultSet rs = database.execQuery(query);
			compte = rs.getInt("compte");
		} catch (SQLException e) {
			logger.fatal("SQLException getting dependances métier : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return compte;
	}

	/**
	 * Liste des objets joins par ordre alphabétique
	 * @return ArrayList des objets
	 */
	public static ArrayList<String> listing(){
		ArrayList<String> list = new ArrayList<String>(5);
		String query = "SELECT metier FROM "+TABLE+" ORDER BY metier ASC";
		try{
			ResultSet rs = database.execQuery(query);
			rs.beforeFirst();
			while(rs.next()){
				list.add(rs.getString("metier"));
			}
		}catch(SQLException e){
			logger.fatal("SQLException listing metier : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		list.trimToSize();
		return list;
	}
		
	/**
	 * @return nombre d'acteurs dans la base
	 */
	public Integer getCount(){
		int count = -1;
		try{
			String query = "SELECT count("+PK+") as compte FROM "+TABLE;
			ResultSet rs = database.execQuery(query);
			rs.first();
			count = rs.getInt("compte");
		}catch(SQLException e){
			logger.fatal("SQLException counting metiers : "+e.getMessage()+" (code "+e.getErrorCode()+")");
			sqlMessage = "Message was : "+e.getMessage();
			sqlError = e.getErrorCode();
		}
		return count;
	}	
	/**
	 * @return Renvoie sqlMessage.
	 */
	public String getSqlMessage() {
		return sqlMessage;
	}

	/**
	 * @param message sqlMessage à définir.
	 */
	public void setSqlMessage(String message) {
		sqlMessage = message;
	}

	/**
	 * @return Renvoie id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id id à définir.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Renvoie métier.
	 */
	public String getMetier() {
		return metier;
	}

	/**
	 * @param strmetier metier à définir.
	 */
	public void setMetier(String strmetier) {
		metier = strmetier;
	}

	/**
	 * @return Renvoie sqlError.
	 */
	public static int getSqlError() {
		return sqlError;
	}

	/**
	 * @return Renvoie affectedRows.
	 */
	public int getAffectedRows() {
		return affectedRows;
	}
}