--
-- Current Database: `recherche_emploi`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `recherche_emploi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `recherche_emploi`;

--
-- Table structure for table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE `etat` (
  `id_etat` int(2) NOT NULL AUTO_INCREMENT,
  `etat` varchar(50) NOT NULL DEFAULT '',
  `commentaire` longtext NOT NULL,
  PRIMARY KEY (`id_etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `etat`
--


/*!40000 ALTER TABLE `etat` DISABLE KEYS */;
LOCK TABLES `etat` WRITE;
INSERT INTO `etat` VALUES (1,'Non renseigné','nécéssaire pour l\'intégrité référentielle de la base'),(2,'Négatif',''),(3,'Interessé','Interéssé, mais pas dans l\'immédiat.\r\nA recontacter ultérieurement.'),(4,'Effectif complet',''),(5,'Petite structure','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `etat` ENABLE KEYS */;


--
-- Table structure for table `historique_recherches`
--

DROP TABLE IF EXISTS `historique_recherches`;
CREATE TABLE `historique_recherches` (
  `id_historique` int(4) NOT NULL AUTO_INCREMENT,
  `id_recherche` int(4) NOT NULL,
  `id_etat` int(2) NOT NULL,
  `date` datetime NOT NULL,
  `commentaires` longtext NOT NULL,
  PRIMARY KEY (`id_historique`),
  KEY `recherche_historique` (`id_recherche`),
  KEY `etat_historique` (`id_etat`),
  CONSTRAINT `etat_historique` FOREIGN KEY (`id_etat`) REFERENCES `etat` (`id_etat`),
  CONSTRAINT `recherche_historique` FOREIGN KEY (`id_recherche`) REFERENCES `recherches` (`id_recherche`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `metiers`
--

DROP TABLE IF EXISTS `metiers`;
CREATE TABLE `metiers` (
  `id_metier` int(2) NOT NULL AUTO_INCREMENT,
  `metier` varchar(50) NOT NULL,
  PRIMARY KEY (`id_metier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metiers`
--


/*!40000 ALTER TABLE `metiers` DISABLE KEYS */;
LOCK TABLES `metiers` WRITE;
INSERT INTO `metiers` VALUES (1,'Librairie'),(2,'Boulangerie'),(3,'Supermarché'),(4,'Papéterie'),(5,'Presse'),(6,'Sandwicherie');
UNLOCK TABLES;
/*!40000 ALTER TABLE `metiers` ENABLE KEYS */;

--
-- Table structure for table `recherches`
--

DROP TABLE IF EXISTS `recherches`;
CREATE TABLE `recherches` (
  `id_recherche` int(4) NOT NULL AUTO_INCREMENT,
  `raison_sociale` varchar(100) NOT NULL DEFAULT '',
  `adresse` longtext NOT NULL,
  `cp` varchar(5) NOT NULL DEFAULT '',
  `ville` varchar(50) NOT NULL DEFAULT '',
  `telephone` varchar(10) NOT NULL DEFAULT '',
  `mail` varchar(50) NOT NULL DEFAULT '',
  `site` varchar(100) NOT NULL DEFAULT '',
  `contact` varchar(50) NOT NULL DEFAULT '',
  `poste_contact` varchar(50) DEFAULT NULL,
  `commentaires` longtext NOT NULL,
  `envoye` int(1) NOT NULL DEFAULT '0',
  `date_creation` date NOT NULL DEFAULT '0000-00-00',
  `date_modif` date NOT NULL DEFAULT '0000-00-00',
  `date_rappel` date NOT NULL DEFAULT '0000-00-00',
  `id_metier` int(3) NOT NULL DEFAULT '0',
  `id_etat` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_recherche`),
  KEY `metier_recherche` (`id_metier`),
  KEY `etat_recherche` (`id_etat`),
  CONSTRAINT `etat_recherche` FOREIGN KEY (`id_etat`) REFERENCES `etat` (`id_etat`) ON UPDATE CASCADE,
  CONSTRAINT `metier_recherche` FOREIGN KEY (`id_metier`) REFERENCES `metiers` (`id_metier`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
